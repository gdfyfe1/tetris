module InferSpec where

import           SpecHelper

spec :: Spec
spec = do
    describe "infer" $ do
        it "infers basic types" $
            infer (Ty tInt) (Ty var0) `shouldBe` Just [(Ty var0, Ty tInt)]

        it "handles tuple types" $
            infer (Ty $ typeOf (True, 'c')) (Ty $ typeOf (Zy, Sy Zy)) `shouldBe`
              Just [(Ty var0, Ty tBool), (Ty var1, Ty tChar)]

        it "handles function types" $
            infer (fun (Ty tBool) (Ty tInt)) (fun (Ty var0) (Ty var1)) `shouldBe`
              Just [(Ty var0, Ty tBool), (Ty var1, Ty tInt)]

        it "return Nothing on type error" $
                    infer (fun (Ty tBool) (Ty tInt)) (Ty $ typeOf (True, 7::Int)) `shouldBe`
                      Nothing
