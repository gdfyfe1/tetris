module ZipperSpec where

import           Data.Maybe (fromJust)
import           SpecHelper


t1 = Node 9 [Leaf 1, Leaf 2, Leaf 3]
t2 = Node 99 [Node 11 [Leaf 1, Leaf 2, Leaf 3],
              Node 12 [Leaf 4, Leaf 5, Leaf 6],
              Node 13 [Leaf 7, Leaf 8, Leaf 9]]
z1 = (t1, [])
z2 = (t2, [])

z3 = (Leaf 1, [Crumb 9 [] [Leaf 2, Leaf 3]])

c1 = Crumb 10 [t1, t1] [t1]

spec :: Spec
spec = do
    describe "Tree.fmap" $ do
        it "satisfies the id functor law" $
            fmap id t1 `shouldBe` t1
        it "satisfies the composition functor law" $
            fmap (even . (*2)) t1 `shouldBe` (fmap even . fmap (*2)) t1

    describe "Crumb.fmap" $ do
        it "satisfies the id functor law" $
            fmap id c1 `shouldBe` c1
        it "satisfies the composition functor law" $
            fmap (even . (*2)) c1 `shouldBe` (fmap even . fmap (*2)) c1

    describe "Tree.Foldable" $ do
        it "folds over values" $
            foldr (+) 0  t1 `shouldBe` 15
        it "respects fmap as it is a functor" $
            foldr (+) 0 (fmap (*2) t1) `shouldBe` (15 * 2)

    describe "dfoldr" $ do
        it "folds over values with the depth of the tree" $
            let foo i x xs = (i, x) : xs
            in dfoldr foo [] t1 0 `shouldBe` [(1, 3), (1, 2), (1, 1), (0, 9)]

    describe "down" $ do
                it "trys to move a zipper down to the given index" $
                    down z1 0 `shouldBe` Just z3
                it "will return nothing if index is out of bounds" $
                    down z1 99 `shouldBe` Nothing
                it "will return nothing if at a leaf" $
                    down z3 0 `shouldBe` Nothing

    describe "goTo" $ do
                it "uses down to move the zipper" $
                    goTo z2 [0] `shouldBe` down z2 0
                it "uses down to move the zipper multiple times" $
                    goTo z2 [0, 1] `shouldBe` down (fromJust $ down z2 0) 1
                it "will return nothing with bad directions" $
                    goTo z2 [0..4] `shouldBe` Nothing

    describe "currTree" $ do
        it "return the top element of a tree" $
            currTree t1 `shouldBe` 9

    describe "currZip" $ do
        it "return the current element of a zipper" $
            currZip z2 `shouldBe` 99
        it "respects position" $
            currZip z3 `shouldBe` 1

    describe "get" $ do
        it "combines goTo and currZip to get an element at an index" $
            get z2 [0, 1] `shouldBe` Just 2
        it "returns nothing if the position doesn't exist" $
            get z2 [0, 7] `shouldBe` Nothing

    describe "up" $ do
        it "moves a zipper up a level" $
            up z3 `shouldBe` z1
        it "if at the top returns itself" $
            up z1 `shouldBe` z1

    describe "downAll" $ do
        it "returns all the subtrees of a zipper in a list" $
            length (downAll z2) `shouldBe` 3

    describe "downlast" $ do
        it "returns the last zipper on the next level" $
            currZip (downLast z2) `shouldBe` 13

    describe "root" $ do
        it "moves a zipper to its root" $
            root z3 `shouldBe` z1

    describe "insert" $ do
        it "sets a tree to be the current focus of the zipper" $
            insert z1 t1 `shouldBe` z1

    describe "addTo" $ do
        it "adds a tree to a zippers list of children" $
           (length . downAll $ addTo z1 t1) `shouldBe` 4

    describe "addSingle" $ do
        it "Sets the focus of the zipper to a single tree" $
           currZip (addSingle z1 6) `shouldBe` 6

    describe "countTree" $ do
        it "counts the elements in a tree" $
           countTree t2 `shouldBe` 13

    describe "countTrees" $ do
        it "counts the elements in a foldable container of trees" $
           countTrees [t2, t2, t2] `shouldBe` 3 * countTree t2

    describe "getY" $ do
        it "calculates how far down the zipper is" $
           getY z3 `shouldBe` 1
        it "starts at 0" $
           getY z1 `shouldBe` 0

    describe "mapZ" $ do
        it "fmap for zippers" $
            mapZ (*2) z2 `shouldBe` asZip (fmap (*2) t2)

    describe "generation" $ do
        it "takes a zipper and returns a list of elements at that level" $
                    length (generation z3) `shouldBe` 3

    describe "depthZ" $ do
        it "returns the current depth of the tree" $
            depthZ z3 `shouldBe` 1

    describe "contains" $ do
        it "returns a list of elements that satisfy a predicate" $
            length (contains (even) t2) `shouldBe` 5
        it "is empty if pre is not met" $
            contains (>999) t2 `shouldBe` []

    describe "yank" $ do
        it "yanks off the children of the current focus, returns the first" $
            yank z1 `shouldBe` ((Leaf 9, []), Just $ Leaf 1)
        it "yank at leaf return (z, Nothing)" $
            yank z3 `shouldBe` (z3, Nothing)

    describe "right and left" $ do
        it "moves the current focus of the zipper left or right" $
            left (right z3) `shouldBe` z3

    describe "back" $ do
        it "sets the focus of the zipper back" $
            back z3 `shouldBe` Just z1
        it "return nothing if there is no further back" $
            back z1 `shouldBe` Nothing

    describe "next" $ do
        it "sets the focus of the zipper to the next" $
            next z1 `shouldBe` Just z3
        it "return nothing if there is no further next" $
            next (downLast z1) `shouldBe` Nothing

    describe "nextPre" $ do
        it "moves to the next focus that satisfies the predicate" $
            currZip (fromJust (nextPre z2 (==8) True)) `shouldBe` 8

    describe "backPre" $ do
        it "moves back to the next focus that satisfies the predicate" $
            currZip (fromJust (backPre (lastZ z2) (==3) True)) `shouldBe` 3

    describe "lastZ" $ do
        it "moves to the last element of the zipper" $
            currZip (lastZ z2) `shouldBe` 9








main :: IO ()
main = hspec spec


























