{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}

module ApplicationSpec where

import           Data.Either (isLeft, fromRight)
import           SpecHelper

v1 = 3 :# 2 :# 1 :# Emp

funcPair' = MkFunction polyPair (Ty tBool :# Emp) (Ty $ typeOf (False, False), [Ty tBool]) "Pair"

d1 = Dynamic typeRep True
d2 = Dynamic typeRep (True, True, True)
d3 = Dynamic typeRep not

spec :: Spec
spec = do
    describe "Vector.fmap" $ do
        it "satisfies the id functor law" $
            fmap id v1 `shouldBe` v1
        it "satisfies the composition functor law" $
            fmap (even . (*2)) v1 `shouldBe` (fmap even . fmap (*2)) v1

    describe "mono" $ do
        it "return an error if the poly function hasn't been loaded" $
            isLeft (mono funcPair) `shouldBe` True
        it "returns a poly once the type is updated" $
            isLeft (mono funcPair')  `shouldBe` False

    describe "run" $ do
        it "takes a function and a list of args and executes" $
            fromDyn (fromRight d1 (run funcPair' [d1])) (False, False)
             `shouldBe` (True, True)

        it "returns an error if there aren't enough arg" $
            isLeft (run funcPair' []) `shouldBe` True

        it "returns an error if there are too many args" $
            isLeft (run funcPair' [d1, d1]) `shouldBe` True

    describe "update" $ do
        it "updates the types of a function" $
            update (Ty var0, Ty tBool) funcPair `shouldBe` funcPair'

        it "prints tuple dynamics" $
            showDyn d2 `shouldBe` "(True, True, True)"

        it "prints the types of any other dynamics" $
            showDyn d3 `shouldBe` "Bool -> Bool"
