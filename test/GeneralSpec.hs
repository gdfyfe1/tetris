module GeneralSpec where

import           SpecHelper

spec :: Spec
spec = do
    describe "imap" $ do
        it "acts as a map but with an increasing index" $
            let foo i x = (i, x)
            in imap foo ['a', 'b','c'] `shouldBe` [(0, 'a')
                                                 , (1, 'b')
                                                 , (2, 'c')]

