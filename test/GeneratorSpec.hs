module GeneratorSpec where

import           SpecHelper
import qualified ZipList         as ZL

mkD1 = MkData dInt (Ty tInt) Nothing

test0 :: Tree Cont
test0 =  Node  (Func funcPreInt)
               [Node  (Func funcNot)
                   [Leaf $ Hole (Ty tBool)],
                Node  (Func funcAddTimes )
                   [Leaf $ Data mkD1,
                    Leaf $ Hole (Ty tInt),
                    Leaf $ Data mkD1]]

testC0 = fmap (convRej False) test0

zl = ZL.ZipList [] (asZip <$>Just testC0) [Nothing, Nothing, Just testC0]

spec :: Spec
spec = do
    describe "zipList" $ do
        it "creates a ziplist from a list of maybe trees" $
            ZL.zipList [Just testC0, Nothing, Nothing, Just testC0] `shouldBe` zl

    describe "getTypes" $ do
            it "gets the types, data" $
                getTypes holePre zl `shouldBe` [Ty tInt, Ty tBool, Ty tInt, Ty tBool]

            it "gets the types, functions" $
                getTypes funcPre zl `shouldBe` [Ty tInt, Ty tBool, Ty tInt, Ty tBool, Ty var0]






