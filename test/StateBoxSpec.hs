module StateBoxSpec where

import           SpecHelper

import           Data.Either (isRight)

mkD2 = MkData dBool (Ty tBool) Nothing

spec :: Spec
spec = do
    describe "runFunction" $ do
        it "performs function if the first arg is a function" $
            isRight (runFunction (Func funcNot) [Data $ mkD2])
                `shouldBe` True

        it "is error if not" $
                isRight (runFunction (Data $ mkD2) [Data $ mkD2])
                    `shouldBe` False