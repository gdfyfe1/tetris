module NattySpec where

import           SpecHelper

spec :: Spec
spec = do
    describe "natAsInt" $ do
        it "treats zero as int" $
            natAsInt Zero `shouldBe` 0

        it "treats n as int" $
            natAsInt (Suc $ Suc $ Suc Zero) `shouldBe` 3

    describe "intAsNat" $ do
        it "inverse of natAsInt" $
            natAsInt (intAsNat 44) `shouldBe` 44

    describe "varName" $ do
        it "creates a variable name from an int" $
            varName 0 `shouldBe` "a"

        it "loops" $
            varName 26 `shouldBe` "ba"








