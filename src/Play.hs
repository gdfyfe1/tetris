{-# LANGUAGE ScopedTypeVariables #-}

{-|
Module      : Play
Description : Play State
Copyright   : (c) Graham Fyfe, 2018

This module describes the main state of the game where gameplay takes place.
As much of the functionality is located elsewhere in an attempt to decouple.
  -}

module Play where

import           Control.Monad                    (unless)
import           Control.Monad.Trans.Except
import           Data.Maybe                       (fromMaybe, fromJust)
import qualified Data.Sequence                    as Seq
import           Graphics.Gloss
import           Graphics.Gloss.Interface.IO.Game

import           Error
import           GameState
import           General
import           Generator
import           Model
import           PicRow
import           Piece
import           Settings
import           StateBox
import qualified ZipList                          as ZL
import           Zipper

--Play Update Function
update :: Float -> PlayState -> IO GameState
update _ ps = do
    let ps' = movePiece ps
    if isCollide ps'
    then do
         res <- runInfer ps'
         return . PlayState $ res
    else return . PlayState $ ps'

-- | Moves the piece based on the current drop rate.
movePiece :: PlayState -> PlayState
movePiece ps | isDrop . optsP $ ps = ps{ pieceX = pieceX ps - dropRate ps }
             | otherwise           = ps

-- | Once a piece has landed, inference is run and the state is updated.
runInfer :: PlayState -> IO PlayState
runInfer ps = case piece ps of
                  Nothing -> return ps
                  Just p  -> do
                        (zl, p', s) <- handleIOErr $ updatePiece (forest $ ps) p
                        return $ redrawBoard $ incScore s $ setBoard zl $ setPiece p' ps

-- | Generates a new piece and then updates the game state.
newPiece :: PlayState -> IO PlayState
newPiece ps = do
            p <- handleIOErr . genPiece . forest $ ps
            return ps {piece = Just p}

setPiece :: Maybe Piece -> PlayState -> PlayState
setPiece p ps = ps { piece  = p,
                     pieceX = 1500 }

setBoard :: ZL.ZipList StateBox -> PlayState -> PlayState
setBoard zl ps = moveBoard $ ps { forest = zl }

-- | Moves the board ensure new piece isn't hovering over an invalid spot.
moveBoard :: PlayState -> PlayState
moveBoard ps = ps {forest = moveHole True (forest ps) (piecePre . piece $ ps) }

incScore :: Int -> PlayState -> PlayState
incScore s ps = ps {score = score ps + s}

-- | Checks if the piece has hit the board
isCollide :: PlayState -> Bool
isCollide ps = pieceX ps <= 0


--Render Constants
treeXOff :: Float
treeXOff = 200


prt = PicRow [text "1", text "2", text "3", text "4", text "5", text "6", text "7", text "8", text "9"] height 5

--Main render function
render :: PlayState -> IO Picture
render ps = do let pr = case cachedBoard ps of
                        Nothing  -> fromJust . cachedBoard . redrawBoard $ ps
                        Just pr' -> pr'
               hud <- rHUD ps
               return $ translate xOff yOff  $
                                   pictures [ rBG,
                                              translate 50 50 $ rPicRowSel (ZL.getY $ forest ps) pr,
                                              translate 50 50 $ offPiece (rMPiece ps) pr,
                                              hud]
-- | Updatess the board cache.
redrawBoard :: PlayState -> PlayState
redrawBoard ps = ps {cachedBoard = Just $ rMTrees ps}
--

-- | Paints the background colour
rBG :: Picture
rBG = renderAt 0.5 0.5 $
        color (makeColorI 173 244 250 255) $
            rectangleSolid (fromIntegral width) (fromIntegral height)

-- | Describes the entire gameboard in terms of a picrow
rMTrees :: PlayState -> PicRow
rMTrees ps = foldl (rMTree (rMode ps))
                   (PicRow [] height (rows . optsP $ ps))
                   (ZL.asList $ forest ps)

-- | Describes a single of the game board
rMTree :: Bool -> PicRow -> Maybe (Tree StateBox) ->  PicRow
rMTree b pr Nothing  = addPic (circleSolid 30) pr
rMTree b pr (Just t) = dfoldr foo pr t 0
    where foo = addPic `com2` rBox b

-- | Describes a single entry on the gameboard with offsets.
rBox :: Bool -> Int -> StateBox -> Picture
rBox b i sb = rStateLine b sb (fromIntegral i * treeXOff) 0


-- | Decribes the falling piece wrapped in a maybe.
rMPiece ::  PlayState -> Picture
rMPiece ps = case piece ps of
                Nothing -> blank
                Just p  -> rPiece (rMode ps) p rx ry
    where rx = pieceX ps +
               treeXOff * fromIntegral (maybe 0 depthZ (ZL.curr . forest $ ps))
          ry = 0

-- | Decribes the falling piece.
rPiece ::  Bool -> Piece -> Float -> Float -> Picture
rPiece b (Reg t)    x y = rStateLine b (currTree t) x y
rPiece _ (Power p)  x y = translate x y $ text $ show p

----HUD RENDERING-------------

-- | Describes the other parts of the game screen, such as the score and sick wood grain.
rHUD :: PlayState -> IO Picture
rHUD ps = do
        bmp <- loadBMP "res/hud.bmp"
        return $
            pictures [ renderAt 0.875 0.5 $ bmp,
                       rScore,
                       rSaved]
    where rScore = renderAt 0.8 0.5  $ scale 0.3 0.3 $
                    text . show . score $ ps
          rSaved = renderAt 0.8 0.15 $ scale 0.3 0.3 $
                    text $ maybe "" show (saved ps)

-- | Describes a single entry on the gameboard.
rStateLine :: Bool -> StateBox -> Float -> Float -> Picture
rStateLine b sb x y = let col = if isRej sb then red else black
                      in color col $ translate x y $ text $ showSB b sb



-- Handle events
handleEvents :: Event -> PlayState -> IO GameState
handleEvents (EventKey (SpecialKey KeyEsc)    Down _ _) ps = return $ playToMenu ps
handleEvents (EventKey (SpecialKey KeySpace)  Down _ _) ps =
    return $ PlayState ( ps{ pieceX = 0})

handleEvents (EventKey (SpecialKey KeyUp)     Down _ _) ps =
   return $ PlayState $ redrawBoard $  ps { forest = moveHole False (forest ps) (piecePre $ piece ps) }

handleEvents (EventKey (SpecialKey KeyDown)   Down _ _) ps =
    return $ PlayState $ redrawBoard $  ps { forest = moveHole True (forest ps) (piecePre $ piece ps) }

handleEvents (EventKey (SpecialKey KeyShiftL) Down _ _) ps =
    return  $ PlayState $ redrawBoard ( ps{ rMode = not $ rMode ps})

handleEvents (EventKey (Char 's') Down _ _) ps = PlayState <$> savePiece ps

handleEvents (EventKey (Char 'p') Down _ _) ps = do
    --print $ holes ps
    return $ PlayState ps

handleEvents (EventKey (Char 't') Down _ _) ps = do
   -- print $ pieceY ps --(holes ps) !! (selHole ps))
    return $ PlayState ps

handleEvents _ ps  = return $ PlayState ps

-- | Implements the piece saving feature. Generates a new one if needed.
savePiece :: PlayState -> IO PlayState
savePiece ps = moveBoard <$> case saved ps of
                                Nothing -> newPiece $ ps { saved = piece ps}
                                Just s  ->  return  $ ps { saved = piece ps,
                                                           piece = Just s }

-- | Utility function for rendering at certain point on the screen.
renderAt :: Float -> Float -> Picture -> Picture
renderAt x y p = translate (fromIntegral width * x) (fromIntegral height * (-y)) p

















