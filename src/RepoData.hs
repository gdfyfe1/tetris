{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE GADTs #-}
{-# OPTIONS_HADDOCK prune#-}
{-|
Module      : RepoData
Description : Data functions repository.
Copyright   : (c) Graham Fyfe, 2018

This module stores the data functions and has functions for generating data
from specific types.
-}

module RepoData where

import           Control.Monad.IO.Class
import           Control.Monad.Trans.Except
import           Data.Char                  (chr, intToDigit, ord)
import           Data.Dynamic
import Control.Monad
import           Data.Kind
import           Data.Maybe
import           System.Random
import           Type.Reflection
import Control.Applicative


import qualified Data.Map                   as Map

import           Application
import           Error
import           General
import           Natty
import           Ty
import           WeightedList
import           Zipper
import           Infer

-- | Given a list types this function tries to generate a falling data piece.
genDataS :: [Ty Type] -> IO (Maybe Data)
genDataS ts =  do
         d <- sequence dat
         case d of
            [] -> return Nothing
            ds -> do i <- choice $ ds
                     return i
    where dat = foldl foo [] ts
          foo xs t = genData t : xs

-- | Given a single type this function tries to generate a falling data piece.
genData :: Ty Type -> IO (Maybe Data)
genData t@(Ty (Fun _ _)) = genDataFunc t
genData ty = do
                (Ty ty') <- varToRand ty
                dat <- randData ty'
                return . Just $ MkData (Dynamic ty' dat) (Ty ty') Nothing

-- | Creates random data to match the passed type rep.
-- | Only works with: Int, Bool, Char, Lists and tuples up to 4
randData :: TypeRep a -> IO a
randData tr = case tr of
    App ls a
        | Just HRefl <- eqTypeRep (typeRep @[]) ls
            -> do
                n <- randomRIO (2, 5)
                replicateM n (randData a)
    App (App comma a) b
        | Just HRefl <- eqTypeRep (typeRep @(,)) comma
            -> liftA2 (,) (randData a) (randData b)
    App (App (App comma a) b) c
        | Just HRefl <- eqTypeRep (typeRep @(,,)) comma
            -> liftA3 (,,) (randData a) (randData b) (randData c)
    App (App (App (App comma a) b) c) d
        | Just HRefl <- eqTypeRep (typeRep @(,,,)) comma
            -> (,,,) <$> (randData a) <*> (randData b) <*>
                         (randData c) <*> (randData d)    --liftA4 sadly doesn't exist
    _ | Just HRefl <- eqTypeRep (typeRep @Int)  tr -> randomRIO (-20, 100)
      | Just HRefl <- eqTypeRep (typeRep @Bool) tr -> randomIO
      | Just HRefl <- eqTypeRep (typeRep @Char) tr -> randomRIO ('a', 'z')
      | otherwise -> fail $ show tr

-- | Given a single type this function tries to generate a data function.
genDataFunc :: Ty Type -> IO (Maybe Data)
genDataFunc t = case filter check allDatFuncs of
                    [] -> return Nothing
                    ds -> choice $ Just <$> ds
    where check f = canInfer t (dataType f)

allDatFuncs = aTobDatFuncs ++ aToaDatFuncs

-- a -> b functions

intToChar    = [MkData (Dynamic typeRep chr')
                    (fun (Ty tInt) (Ty tChar)) (Just "Char"),
                MkData (Dynamic typeRep intToDigit)
                    (fun (Ty tInt) (Ty tChar)) (Just "Digit")]

intToBool    = [MkData (Dynamic typeRep even')
                    (fun (Ty tInt) (Ty tBool)) (Just "Even"),
                MkData (Dynamic typeRep neg)
                    (fun (Ty tInt) (Ty tBool)) (Just "Negative")]

charToInt    = [MkData (Dynamic typeRep ord')
                    (fun (Ty tChar) (Ty tInt)) (Just "Ord")]

charToBool   = [MkData (Dynamic typeRep graham)
                    (fun (Ty tChar) (Ty tBool)) (Just "Graham")]

boolToInt    = [MkData (Dynamic typeRep bit)
                    (fun (Ty tBool) (Ty tInt)) (Just "Bit")]

boolToChar    = [MkData (Dynamic typeRep bit)
                    (fun (Ty tBool) (Ty tInt)) (Just "Bit")]

aTobDatFuncs = intToChar ++ intToBool ++ charToInt ++ charToBool ++ boolToInt ++ aToaDatFuncs


chr' :: Int -> Char
chr' x = chr $ ((x + 20) `mod` 26) + 97

neg :: Int -> Bool
neg x = x < 0

even' :: Int -> Bool
even' = even

ord' :: Char -> Int
ord' c = ord c - 97

graham :: Char -> Bool
graham c = c `elem` "graham"

bit :: Bool -> Int
bit b = if b then 1 else 0

boolC :: Bool -> Char
boolC b = if b then 't' else 'f'


-- a -> a functions

intToInt = [MkData (Dynamic typeRep double) (fun (Ty tInt) (Ty tInt)) (Just "Double"),
            MkData (Dynamic typeRep square) (fun (Ty tInt) (Ty tInt)) (Just "Square"),
            MkData (Dynamic typeRep minTen) (fun (Ty tInt) (Ty tInt)) (Just "- Ten")]

charToChar = [MkData (Dynamic typeRep nextC) (fun (Ty tChar) (Ty tChar)) (Just "Next")]

boolToBool = [MkData (Dynamic typeRep not)  (fun (Ty tBool) (Ty tBool)) (Just "Not" )]

aToaDatFuncs = intToInt ++ charToChar ++ boolToBool


double :: Int -> Int
double x = x * 2

square :: Int -> Int
square x = x * x

minTen :: Int -> Int
minTen x = x - 10

nextC :: Char -> Char
nextC c =  chr' $ (ord' c + 1) - 20

-- a -> b -> b

add :: Int -> Int -> Int
add x y = x + y

























