{-|
Module      : Generator
Description : Base module for generation.
Copyright   : (c) Graham Fyfe, 2018

This module connects together the data and function repositories and provides
functions for the generation of the game's falling pieces.
-}

module Generator where

import           Control.Monad
import           Control.Monad.IO.Class
import           Control.Monad.Trans.Except
import           Data.Kind
import           System.Random
import           Data.List                  (nub)

import           WeightedList
import           Application
import           Error
import           Piece
import           RepoData
import           RepoFunction
import           StateBox
import           Ty
import           Zipper
import qualified ZipList as ZL

-- | Creates a weighted list element for a potential data piece.
addData :: ZL.ZipList StateBox -> IO [WEle Piece]
addData zl = do
            ds <- genDataS $ getTypes holePre zl
            case ds of
                Nothing -> return []
                Just d  -> return [(10, mkPiece d)]

-- | Creates a weighted list element for a potential function piece.
addFunc :: ZL.ZipList StateBox -> IO [WEle Piece]
addFunc zl = do
            fs <- genFunc $ getTypes funcPre zl
            case fs of
                Nothing -> return []
                Just f  -> return [(8, mkPiece f)]

-- | Creates a weighted list element for a potential yank piece.
addYank :: ZL.ZipList StateBox -> IO [WEle Piece]
addYank zl = case getTypes rejPre zl of
                [] -> return []
                _  -> return [(10, Power Yank)]

-- | Creates a weighted list element for a potential fill piece.
addFill :: ZL.ZipList StateBox -> IO [WEle Piece]
addFill zl = case getTypes holePre zl of
                [] -> return []
                _  -> return [(1, Power Fill)]

-- | Uses all of the previous functions to create a weighted list
-- | from which the piece is selected.
genPiece :: ZL.ZipList StateBox -> EitherIO Piece
genPiece zl = do
             pd <- liftIO $ addData zl
             pf <- liftIO $ addFunc zl
             py <- liftIO $ addYank zl
             pg <- liftIO $ addFill zl
             res <- liftIO $ selRand $ pd ++ pf ++ py ++ pg
             case res of
                Nothing -> throwE $ ErrorNoFunction "sadsad"
                Just r  -> return r

-- | Creates data that can fill an argument regardless of type.
fillPiece :: ZL.ZipList StateBox -> IO (Maybe (Tree StateBox))
fillPiece zl = case ZL.currCurr zl of
                   Nothing -> return Nothing
                   Just x  -> do
                            md <- genData $ getType x
                            return $ Leaf <$> convRej False <$> Data <$> md

-- | Give a predicate, calculates appropriate types for generated pieces.
getTypes :: (Bool, StateBox -> Bool) -> ZL.ZipList StateBox -> [Ty Type]
getTypes (b, pre) zl = let xs = map getType (ZL.contains pre zl)
                           nt = if b && ZL.hasNot zl
                               then [Ty var0]
                               else []
                       in xs ++ nt
