{-# LANGUAGE ScopedTypeVariables #-}

{-|
Module      : PicRow
Description : Combines several pictures in a stack.
Copyright   : (c) Graham Fyfe, 2018

This module provides a method for combining pictures on top of each other.
When created, the height and the number columns which facilitates the scrolling
of the main game board.
  -}

module PicRow where

import           General
import           Graphics.Gloss

-- | Describes several pictures atop each other witha set height and no of cols.
data PicRow = PicRow [Picture] Int Int

-- | Adds a new picture to a pic row
addPic :: Picture -> PicRow -> PicRow
addPic p (PicRow ps h r) = PicRow (p : ps) h r

-- | Combines all of the pictures into a single picture and
-- | then offsets it based on the passed int.
rPicRowSel :: Int -> PicRow -> Picture
rPicRowSel sel pr@(PicRow pics h r) =
              rPROff ((fromIntegral ((r `div` 2) - sel) * offsetPR pr * (-1)) - (offsetPR pr / 2) ) pr

-- | Combines all of the pictures into a single picture
rPicRow :: PicRow -> Picture
rPicRow pr = rPROff (offsetPR pr / (-2)) pr

-- | Performs the work of the previous two functions
rPROff :: Float -> PicRow -> Picture
rPROff y pr@(PicRow pics h r) = translate 0 y $ pictures $ imap adjust (reverse pics)
    where adjust (i :: Int) p = translate 0 (fromIntegral i * offsetPR pr * (-1)) $
                                scale (scalePR pr) (scalePR pr) p


-- | Translates the selection triangle to the correct row
yPiece :: Picture -> PicRow -> Int -> Picture
yPiece p pr i = translate 0 ((fromIntegral i * offsetPR pr * (-1)) - (offsetPR pr / 2)) $
                   scale (scalePR pr) (scalePR pr) p

-- | Calculates the games boards y offset
offPiece :: Picture -> PicRow -> Picture
offPiece p pr = translate 0 (centRow pr) $ scale (scalePR pr) (scalePR pr) p

-- | The offset of a single row
offsetPR :: PicRow -> Float
offsetPR (PicRow _ h r) = fromIntegral h / fromIntegral r

centRow :: PicRow -> Float
centRow pr@(PicRow _ h r) =  (fromIntegral (r `div` 2) * offsetPR pr * (-1)) - (offsetPR pr / 2)

arb = 0.005
scalePR :: PicRow -> Float
scalePR (PicRow _ h r) = fromIntegral h / fromIntegral r * arb
