{-|
Module      : Menu
Description : Menu State.
Copyright   : (c) Graham Fyfe, 2018

This module is the main menu state of the game where the play and
options state can be accessed.
-}

module Menu where

import           Control.Monad                    (when)
import           Graphics.Gloss
import           Graphics.Gloss.Interface.IO.Game
import           System.Exit                      (exitSuccess)

import           GameState
import           General
import           PicRow

update :: Float -> MenuState -> IO GameState
update dt ms = do
            when (isFinished ms) exitSuccess
            return $ MenuState ms

--render constants
textScale    = 0.5
selCol       = dark red
unselCol     = black
sOff, tsOff :: Float
sOff         = 75
tsOff        = 80


render :: MenuState -> IO Picture
render ms = return $ pictures [rBG,
                               rHUD ms]
    where rBG = color (makeColorI 173 244 250 255) $
                rectangleSolid (fromIntegral width) (fromIntegral height)

-- | Combines the menu options and the red triangle into a picture.
rHUD :: MenuState -> Picture
rHUD ms = let pr = rOpts ms
          in  translate (xOff + 200) (yOff - 100) $
                        pictures [rPicRow pr,
                                  yPiece rTri pr (selMenu ms)]

-- | Describes the options list as PicRow.
rOpts ::  MenuState -> PicRow
rOpts ms = foldl rOpt
                 (PicRow [] 300 5)
                 (menuOptions ms)

-- | Describes a single option.
rOpt :: PicRow -> (MenuState -> GameState, String) ->  PicRow
rOpt pr opt = addPic (text $ snd opt) pr

-- | Describes the red selection triangle.
rTri = translate (-100) 0 $ color selCol $ polygon [(0, 0),
                                                   (0, tsOff),
                                                   (0 + tsOff, tsOff / 2)]


-- Handle events
handleEvents :: Event -> MenuState -> IO GameState
handleEvents (EventKey (SpecialKey KeyUp)    Down _ _) ms = return $ MenuState $ changeOpt (-1) ms
handleEvents (EventKey (SpecialKey KeyDown)  Down _ _) ms = return $ MenuState $ changeOpt   1 ms
handleEvents (EventKey (SpecialKey KeySpace) Down _ _) ms = return $
                                                           fst (menuOptions ms !! selMenu ms) ms
handleEvents (EventKey (SpecialKey KeyEsc)   Down _ _) ms = return $ exitGame ms
handleEvents _ ms = return $ MenuState ms

-- | Changes the current option either up or down.
changeOpt :: Int -> MenuState -> MenuState
changeOpt n ms = ms {selMenu = ((+ n) . selMenu $ ms) `mod` (length . menuOptions $ ms)}

























