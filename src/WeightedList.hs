{-|
Module      : WeightedList
Description : Simple weighted list implementation.
Copyright   : (c) Graham Fyfe, 2018

This module contains a simple weighted list implementation
  -}

module WeightedList where

import System.Random

type WEle  a = (Int, a)
type WList a = [WEle a]

-- | Selects a element from a WList but respects their weights in terms of probability
selRand :: WList a -> IO (Maybe a)
selRand list = do
            rnd <- randomRIO (1, countL)
            return $ help list rnd
    where countL = foldl (\ n (i, _) -> n + i) 0 list
          help [] _                       = Nothing
          help ((n, x): xs) i | i <= n    = Just x
                              | otherwise = help xs (i - n)


