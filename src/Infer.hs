{-|
Module      : Infer
Description : Inference functions.
Copyright   : (c) Graham Fyfe, 2018

This module has the functions that perform the games inference.
-}

module Infer where

import           Data.Kind
import           Data.Maybe
import           Type.Reflection

import           Natty
import           Ty

-- | Checks if two types will cause a type error if combined.
canInfer :: Ty Type -> Ty Type -> Bool
canInfer x y = isJust $ infer x y

-- | Takes two types are runs type inference on them.
-- | The result is a type map from variables to their new type or a type fail.
infer :: Ty Type -> Ty Type -> Maybe [TypeMap]
infer (Ty x) (Ty y) = do
            let tm = isVar (Ty x) (Ty y)
            case tm of
                [] -> check (splitApps x) (splitApps y)
                _  -> return tm
    where isVar s t | isNatty s = [(s, t)]
                    | isNatty t = [(t, s)]
                    | otherwise = []
          check (c, rep) (c', rep')  | c == c'   = inferMult (extract rep) (extract rep')
                                     | otherwise = Nothing
          extract = mapMaybe extract'
          extract' (SomeTypeRep q) = toType q

-- | Performs the infer of lists of types.
-- | InferMult is used as a helper function to the main infer.
inferMult :: [Ty Type] -> [Ty Type] -> Maybe [TypeMap]
inferMult [] [] = return []
inferMult _  [] = Nothing
inferMult [] _  = Nothing
inferMult (s : ss) (t : ts) = do
                tm  <- infer s t
                res <- inferMult (updateMult tm ss) (updateMult tm ts)
                return $ tm ++ res

