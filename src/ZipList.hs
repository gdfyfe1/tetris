{-|
Module      : ZipList
Description : Several zippers combined.
Copyright   : (c) Graham Fyfe, 2018

This module describes the behaviour of Ziplists, a method of combining several zippers
together. There are also many functions which control the current focus of the zippers.
-}


module ZipList where

import Data.Maybe (isNothing)

import qualified Zipper as Z

-- | A Ziplist has a central zipper, a list of trees to the left and
-- | a list of trees to the right.
data ZipList a = ZipList [Maybe (Z.Tree a)] (Maybe (Z.Zipper a)) [Maybe (Z.Tree a)]
    deriving (Show, Eq)


-- Cluncky as I had issues making zipper a functon
instance Functor ZipList where
    fmap f (ZipList ls mz rs) = let mz' = case mz of
                                            Nothing      -> Nothing
                                            Just (t, bs) -> Just (fmap f t, map (fmap f) bs)
                                in ZipList (map (fmap . fmap $ f) ls)
                                            mz'
                                           (map (fmap . fmap $ f) rs)


-- | Constructor function
zipList :: [Maybe (Z.Tree a)] -> ZipList a
zipList (x : xs) = ZipList  [] (Z.asZip <$> x) xs
zipList []       = ZipList  [] Nothing       []


-- | Shifts to the zipper on the right, warps if needed
shiftR :: ZipList a -> ZipList a
shiftR (ZipList ls       mz (r : rs)) =
    ZipList (ls ++ [Z.asTree <$> mz]) (Z.asZip <$> r)  rs
shiftR (ZipList (l : ls) mz [])       =
    ZipList  []                     (Z.asZip <$> l) (ls ++ [Z.asTree <$> mz])
shiftR zl                           = zl

-- | Shifts to the zipper on the left, warps if needed
shiftL :: ZipList a -> ZipList a
shiftL (ZipList ls mz rs) =
    case ls of
        [] -> case rs of
            [] -> ZipList ls mz rs
            _  -> ZipList ((Z.asTree <$> mz) : init rs) (Z.asZip <$> last rs)  []
        _  -> ZipList (init ls) (Z.asZip <$> last ls)  ((Z.asTree <$> mz) : rs)

-- | Goes to next spot on the current zipper or shifts if at the end
next :: ZipList a -> ZipList a
next (ZipList ls  Nothing rs) = shiftR (ZipList ls Nothing rs)
next (ZipList ls (Just z) rs) = case Z.next z of
                                    Nothing -> shiftR (ZipList ls (Just z) rs)
                                    jn      ->           ZipList ls jn       rs

-- | Goes back a spot on the current zipper or shifts if at the end
back :: ZipList a -> ZipList a
back (ZipList ls  Nothing rs) = setLast $ shiftL (ZipList ls Nothing rs)
back (ZipList ls (Just z) rs) = case Z.back z of
                                    Nothing -> setLast $ shiftL (ZipList ls (Just z) rs)
                                    jn      ->                   ZipList ls jn       rs

-- | Like next but next spot must satisfy a predicate
nextPre :: Bool -> (a -> Bool) -> ZipList a -> Maybe (ZipList a)
nextPre = movePre next

-- | Like back but next spot must satisfy a predicate
backPre :: Bool -> (a -> Bool) -> ZipList a -> Maybe (ZipList a)
backPre = movePre back

-- | Implements the predicate moves based on a passed strategy
movePre :: (ZipList a -> ZipList a) -> Bool -> (a -> Bool) ->
    ZipList a -> Maybe (ZipList a)
movePre mv b p start = foo (count start) start
    where foo 0 _   = Nothing
          foo i zl  = case mv zl of
                       (ZipList ls Nothing rs)
                           -> if b
                              then Just $ ZipList ls Nothing rs
                              else foo (i - 1) (ZipList ls Nothing rs)
                       (ZipList ls (Just mz) rs)
                           -> if p $ Z.currZip mz
                              then Just $ ZipList ls (Just mz) rs
                              else foo (i - 1) (ZipList ls (Just mz) rs)




-- | How many elements are in the whole zip list?
count :: ZipList a -> Int
count (ZipList ls mz rs) = foldr foo 0 ls + foo (Z.asTree <$> mz) 0 + foldr foo 0 rs
    where foo mt n = n + maybe 1 Z.countTree mt

setZip :: ZipList a -> Maybe (Z.Zipper a) -> ZipList a
setZip (ZipList ls _ rs) mz = ZipList ls mz rs

curr :: ZipList a -> Maybe (Z.Zipper a)
curr (ZipList _ mz _) = mz

currCurr :: ZipList a -> Maybe a
currCurr zl = Z.currZip <$> curr zl

-- | Set current zipper to focus on last element
setLast :: ZipList a -> ZipList a
setLast (ZipList ls mz rs) = ZipList ls (Z.lastZ <$> mz) rs

asList :: ZipList a -> [Maybe (Z.Tree a)]
asList (ZipList ls mz rs) = ls ++ ((Z.asTree <$> mz) : rs)

-- | Rendered as a stack, how far down is the current focus?
getY :: ZipList a -> Int
getY (ZipList ls mz _) = foldr foo 0 ls + maybe 0 Z.getY mz
    where foo mt n = n + maybe 1 Z.countTree mt

atNot :: ZipList a -> Bool
atNot (ZipList _ Nothing _) = True
atNot  _                    = False

contains :: (a -> Bool) -> ZipList a -> [a]
contains p zl = foldl foo [] (asList zl)
    where foo xs z = xs ++ maybe [] (Z.contains p) z

hasNot :: ZipList a -> Bool
hasNot zl = or $ map isNothing (asList zl)





















