{-|
Module      : Settings
Description : Settings which used by the options state.
Copyright   : (c) Graham Fyfe, 2018

This module describes some functions and types which are essential for the options menu.
-}
module Settings where

import           Data.Maybe    (fromMaybe)
import qualified Data.Sequence as Seq

-- | A record of the different options in the system.
data Options = Options { rows   :: Int,
                         isDrop :: Bool}

-- | Different options menus have different value types.
data ListOpt = OptBool Bool String | OptInt IntRange String deriving Show

-- Cur Min Max Inc
-- | Int range describes the behaviour of an integer based option.
-- | Current, mic, max, increments
data IntRange = IntRange Int Int Int Int deriving Show

stdOption = Options 12 False

-- | Describes how a list option should be updated on a left button press.
left :: ListOpt -> ListOpt
left (OptBool b s) = OptBool (not b) s
left (OptInt ir@(IntRange x min max i) s) = let x' = x - i
                                          in OptInt (setIR ir (help x')) s
     where help n | n < min   = x + (max - min)
                  | otherwise = n

-- | Describes how a list option should be updated on a right button press.
right :: ListOpt -> ListOpt
right (OptBool b s) = OptBool (not b) s
right (OptInt ir@(IntRange x min max i) s) = let x' = x + i
                                             in OptInt (setIR ir (help x')) s
     where help n | n > max   = n - (max - min) - 1
                  | otherwise = n

-- | Sets the value of an int option
setIR ::   IntRange -> Int -> IntRange
setIR  (IntRange cur min max inc) new = IntRange new min max inc

-- | Gets the int from an int range
fromIR :: IntRange -> Int
fromIR (IntRange cur _ _ _) = cur

-- | Gets the actual boolean value used for options
extractBool :: Seq.Seq ListOpt -> Int -> Maybe Bool
extractBool seq i = do
                lo <- i `Seq.lookup` seq
                case lo of
                    OptBool b _ -> Just b
                    _           -> Nothing

-- | Gets the actual int value used for options
extractInt :: Seq.Seq ListOpt -> Int -> Maybe Int
extractInt seq i = do
                lo <- i `Seq.lookup` seq
                case lo of
                    OptInt ir _ -> Just $ fromIR ir
                    _           -> Nothing

-- | Extracts the options from their constructs for later use
extractOptions :: Seq.Seq ListOpt -> Options
extractOptions lo = Options
                    (fromMaybe 12   (extractInt  lo 0))
                    (fromMaybe True (extractBool lo 1))


insertOptions :: Options -> Seq.Seq ListOpt
insertOptions opts = Seq.fromList [ OptInt (IntRange (rows opts) 8 18 1) "Rows",
                                    OptBool        (isDrop opts)         "Drop"]








