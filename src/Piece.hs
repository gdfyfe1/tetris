{-|
Module      : Piece
Description : Falling game pieces.
Copyright   : (c) Graham Fyfe, 2018

This module has the structure of what a falling piece can be along
with some utilty functions.
  -}

module Piece where

import           Application
import           StateBox
import           Zipper

-- | A falling piece is either regualr or a power-up
data Piece = Reg (Tree StateBox) | Power Power  deriving  Eq

-- | Various special falling pieces
data Power = Yank | Fill | Kill   deriving (Show, Eq)

instance Show Piece where
    show (Reg t)   = showSB True $ currTree t
    show (Power p) = show p

class AsPiece a where
    mkPiece :: a -> Piece

instance AsPiece Data where
    mkPiece d = Reg $ Leaf $ Cont (Data d) False

instance AsPiece Function where
    mkPiece f@(MkFunction _ _ (_, ts) _) =
            Reg $ Node (Cont (Func f) False) (map conv ts)
        where conv = Leaf . convRej False . Hole

-- | When moving or generating, for each piece should
-- | seeds be included and what predicate should be used.
piecePre :: Maybe Piece -> (Bool, StateBox -> Bool)
piecePre Nothing                              = wildPre
piecePre (Just (Reg t)) | isFunc $ currTree t = funcPre
                        | otherwise           = holePre
piecePre (Just (Power Yank))                  = rejPre
piecePre (Just (Power Fill))                  = holePre

wildPre = (True,  const True)
funcPre = (True , isReady)
holePre = (False, isReady)
rejPre  = (False, isRej)
