{-# LANGUAGE TupleSections #-}

{-|
Module      : Zipper
Description : Trees with a focus.
Copyright   : (c) Graham Fyfe, 2018

This module implements Zippers. Zippers are like trees but are focused on a
specific part.
-}

module Zipper where

import qualified Data.Sequence as Seq

import           Data.Foldable
import           Data.Maybe    (catMaybes, fromMaybe)

import           General

-- | List tree implentation.
data Tree a    = Leaf a | Node a  [Tree a]  deriving (Show, Eq)

-- | Crumbs used to keep track of the position in the tree.
data Crumb a   = Crumb a [Tree a] [Tree a]  deriving (Show, Eq)

type Zipper a = (Tree a, [Crumb a])

instance Functor Tree where
    fmap f (Leaf x)    = Leaf (f x)
    fmap f (Node x xs) = Node (f x) (map (fmap f) xs)

instance Functor Crumb where
    fmap f (Crumb x ls rs) = Crumb (f x) (map (fmap f) ls) (map (fmap f) rs)

instance Foldable Tree where
    foldr f acc (Leaf x)    =  f x acc
    foldr f acc (Node x xs) =  help (f x acc) xs
        where help = foldl (foldr f)

-- | A version of foldr that keeps track of the depth of the tree.
dfoldr :: (Int -> a -> b -> b) -> b -> Tree a -> Int -> b
dfoldr f acc (Leaf x)    i = f i x acc
dfoldr f acc (Node x xs) i = help (f i x acc) xs
    where help = foldl (\ n t -> dfoldr f n t (i + 1))

-- | Tries to move down the zipper, returns nothing if at a leaf
down :: Zipper a -> Int -> Maybe (Zipper a)
down (Leaf x,    bs) i             = Nothing
down (Node x xs, bs) i             = do
                                     curr <- lookup
                                     return (curr, Crumb x (fst split) (tail $ snd split):bs)
    where split = splitAt i xs
          lookup
           | i >= 0 && i < length xs = Just $ xs !! i
           | otherwise               = Nothing

-- | Tries to go to the given location
goTo :: Zipper a -> [Int] -> Maybe (Zipper a)
goTo zip  []    =  Just zip
goTo zip (d:ds) =  do
                    next <- down zip d
                    goTo next ds

-- | Return top element of current focus of zipper
currZip :: Zipper a -> a
currZip zip = currTree $ fst zip

-- | Return top element of the tree
currTree :: Tree a -> a
currTree (Leaf x)   = x
currTree (Node x _) = x

-- | Return the element at the location given by list
get :: Zipper a -> [Int] -> Maybe a
get zip ds = currZip <$> goTo zip ds

-- | Moves up the tree
up :: Zipper a -> Zipper a
up (tree, Crumb x ls rs:bs) = (Node x (ls ++ [tree] ++ rs), bs)
up (tree, [])               = (tree, [])

-- | Returns a list of all possible down moves
downAll :: Zipper a -> [Zipper a]
downAll     (Leaf x,    bs)   = []
downAll zip@(Node x xs, bs)   = catMaybes [ down zip i | i <- [0..(length xs - 1)]]

-- | Goes to the last possible down move
downLast :: Zipper a -> Zipper a
downLast (Leaf x,    bs) = (Leaf x, bs)
downLast (Node x [], bs) = (Node x [], bs)
downLast z               = last $ downAll z

-- | moves to the root of the zipper
root :: Zipper a -> Zipper a
root (tree, Crumb x ls rs:bs) = root (Node x (ls ++ [tree] ++ rs), bs)
root (tree, [])               = (tree, [])

-- | Inserts a tree at the current focus
insert :: Zipper a  -> Tree a -> Zipper a
insert (_, bs) t = (t, bs)

--What to do when adding onto a node?
-- | Adds a tree atop to the current focus
addTo :: Zipper a -> Tree a -> Zipper a
addTo (Leaf x,    bs) t = (Node x [t], bs)
addTo (Node x xs, bs) t = (Node x (t:xs), bs)

addSingle :: Zipper a -> a -> Zipper a
addSingle (_, bs) x = (Leaf x, bs)

--make better if time
countMTrees :: Foldable f => f (Maybe (Tree a)) -> Int
countMTrees = foldr (\ t n -> maybe 1 countTree t + n) 0

countTree :: Tree a -> Int
countTree = length . toList

countTrees :: Foldable f => f (Tree a) -> Int
countTrees = foldr (\ t n -> countTree t + n) 0

-- | Calcuates the yOffset for rendering purposes
getY :: Zipper a -> Int
getY zip@(tree, Crumb x ls rs:bs) = getY (up zip) +  countTrees ls + 1
getY (tree, []) = 0

asTree :: Zipper a -> Tree a
asTree = fst . root

asZip :: Tree a -> Zipper a
asZip x = (x, [])

mapZ :: (a -> b) -> Zipper a -> Zipper b
mapZ f (tree, bs) = (fmap f tree, map (fmap f) bs)

-- | Gets all siblings of zipper + itself
generation :: Zipper a -> [a]
generation x = getGen $ up x
    where getGen (Node y ys, _) = map currTree ys
          getGen  _             = []

-- | Gets the current depth of the zipper
depthZ :: Zipper a -> Int
depthZ (_, bs) = length bs

contains :: (a -> Bool) -> Tree a -> [a]
contains p t = foldl foo [] t
    where foo xs x = xs ++ if p x then [x] else []

-- | Used by yank power-up,removes from the current focus down
yank :: Zipper a -> (Zipper a, Maybe (Tree a))
yank z@(Leaf _, _) = (z, Nothing)
yank   (Node x xs, bs) = ((Leaf x, bs), Just $ xs !! 0)

right :: Zipper a -> Zipper a
right (x, Crumb t ls (r : rs) : bs) = (r, Crumb t (ls ++ [x]) rs : bs)
right z                             = z

--Uses last and init pretty inefficient
left :: Zipper a -> Zipper a
left z@(x, Crumb t ls rs : bs) =
    case ls of
        [] -> z
        _  -> (last ls, Crumb t (init ls) (x : rs) : bs)
left z                         = z

next :: Zipper a -> Maybe (Zipper a)
next z@(Node x ys, _) = down z 0
next z = help z
    where help z@(x, Crumb t ls (r : rs) : bs) = Just $ right z
          help z@(x, Crumb t ls []       : bs) = help $ up z
          help z@(x, [])                       = Nothing

back :: Zipper a -> Maybe (Zipper a)
back z@(_, [])                      = Nothing
back z@(Leaf x, Crumb t ls rs : bs) =
    case ls of
        [] -> Just $ up z
        _  -> Just . lastZ $ left z
back z = help z
    where help z@(x, Crumb t ls rs : bs) = case ls of
                                               [] -> help $ up z
                                               _  -> Just . lastZ $ left z
          help (x, [])                  = Nothing

--check start = cs
movePre ::  (Zipper a -> Maybe (Zipper a)) -> Zipper a  ->
            (a -> Bool) -> Bool -> Maybe (Zipper a)
movePre mv z pre cs | cs         = check z
                    | otherwise  = do
                                 mvz <- mv z
                                 check mvz
    where check z' | pre $ currZip z' = return z'
                   | otherwise        = movePre mv z' pre False


nextPre :: Zipper a -> (a -> Bool) -> Bool -> Maybe (Zipper a)
nextPre = movePre next

backPre :: Zipper a -> (a -> Bool) -> Bool -> Maybe (Zipper a)
backPre = movePre back

checkPre :: Zipper a -> (a -> Bool) -> Bool
checkPre z p = p $ currZip z

--takes a tree and creates a zipper at the last element
lastT :: Tree a -> Zipper a
lastT t = lastZ $ asZip t

lastZ :: Zipper a -> Zipper a
lastZ (Leaf x, bs) = (Leaf x, bs)
lastZ z            = lastZ $ downLast z













