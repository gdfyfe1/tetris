{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
{-# OPTIONS_HADDOCK prune#-}

{-|
Module      : RepoFunction
Description : Functions repository.
Copyright   : (c) Graham Fyfe, 2018

This module stores the main functions that the game uses to build the trees.
-}

module RepoFunction where

import           Control.Monad.IO.Class
import           Control.Monad.Trans.Except
import           Data.Dynamic
import           Data.Kind
import           Data.List                  (nub)
import           Data.Maybe
import           System.Random
import           Type.Reflection

import qualified Data.Map                   as Map

import           Application
import           Error
import           General
import           Infer
import           Natty
import           Ty
import           WeightedList
import           Zipper
import           ZipList
import           Piece
import           StateBox

-- | Given a list types this function tries to generate a falling function piece.
genFunc :: [Ty Type] -> IO (Maybe Function)
genFunc ts = case genFuncs ts of
                [] -> return Nothing
                fs -> choice $ Just <$> fs

-- | Given a list types this function offers all available functions that output those types.
genFuncs :: [Ty Type] -> [Function]
genFuncs ts = foldl help [] ts
    where help xs t =  xs ++ filter (check t) allFuncs
          check t f = canInfer t (funType f)

allFuncs = singleFuncs ++ listFuncs ++ pairaaFuncs ++ pairabFuncs

intFuncs  = [funcAddTimes, funcPreInt, funcTimes]
boolFuncs = [funcNot, funcAnd]

singleFuncs = intFuncs ++ boolFuncs ++
                [funcApp]--, funcFoldr]

listFuncs = [funcMap, funcReverse]

pairaaFuncs = [funcPair]

pairabFuncs = [funcGroup]




-----------------------------TYPES-------------------------------------------------------------


-- tup2 :: Ty -> Ty -> Ty
-- tup2 (Ty x) (Ty y) = Ty $ App (typeOf) y
-- tup3 :: Ty -> Ty -> Ty -> Ty
-- tup3 (Ty x) (Ty y) (Ty z) = Ty $ Fun x $ Fun y z

----------------------------INITIAL FUNCTIONS----------------------------------------------------

-- 0 forall
-- ****************************************
repoAddTimes :: Int -> Int -> Int -> Int
repoAddTimes x y z = x + y * z

-- not

repoPreInt :: Bool -> Int -> Int
repoPreInt pre x | pre       = x
                 | otherwise = x * 2

repoTimes :: Int -> Int -> Int
repoTimes = (*)

--and

-- ****************************************

-- 1 forall
-- ****************************************
repoPair :: forall a. a -> (a, a)
repoPair x = (x, x)

repoApp :: forall a. (a -> a) -> a -> a
repoApp f = f

--reverse

-- ****************************************

-- 2 forall
-- ****************************************
repoGroup :: forall a b. a -> b -> (a, b)
repoGroup x y = (x, y)

-- repoFoldr :: forall a b. (a -> b -> b) -> b -> [a] -> b
-- repoFoldr f acc (x : xs) = repoFoldr f (f x acc) xs
-- repoFoldr f acc []       = acc

-- map


-- ****************************************

--------------------------GARNISHED FUNCTIONS----------------------------------------------------

-- 0 forall
-- ****************************************
polyAddTimes = Mono (Dynamic typeRep repoAddTimes) 3
polyNot      = Mono (Dynamic typeRep not) 1
polyPreInt   = Mono (Dynamic typeRep repoPreInt) 2
polyTimes    = Mono (Dynamic typeRep repoTimes) 2
polyAnd      = Mono (Dynamic typeRep (&&)) 2
-- ****************************************

-- 1 forall
-- ****************************************
polyPair     = All $ \ (Ty (a :: TypeRep ta)) -> Mono (Dynamic (withTypeable a typeRep) (repoPair @ta)) 1
polyApp      = All $ \ (Ty (a :: TypeRep ta)) -> Mono (Dynamic (withTypeable a typeRep) (repoApp @ta)) 2
polyReverse  = All $ \ (Ty (a :: TypeRep ta)) -> Mono (Dynamic (withTypeable a typeRep) (reverse @ta)) 1
-- ****************************************

-- 2 forall
-- ****************************************
polyGroup    = All $ \ (Ty (a :: TypeRep ta)) -> All $ \ (Ty (b :: TypeRep tb)) ->
                     Mono (Dynamic (withTypeable b (withTypeable a typeRep)) (repoGroup @ta @tb)) 2
-- polyFoldr    = All $ \ (Ty (a :: TypeRep ta)) -> All $ \ (Ty (b :: TypeRep tb)) ->
--                      Mono (Dynamic (withTypeable b (withTypeable a typeRep)) (repoFoldr @ta @tb)) 3
polyMap      = All $ \ (Ty (a :: TypeRep ta)) -> All $ \ (Ty (b :: TypeRep tb)) ->
                     Mono (Dynamic (withTypeable b (withTypeable a typeRep)) (map @ta @tb)) 2
-- ****************************************

------------------------FINAL FUNCTIONS----------------------------------------------------

-- 0 forall
-- ****************************************
funcAddTimes = MkFunction polyAddTimes Emp (Ty tInt,  [Ty tInt, Ty tInt, Ty tInt]) "Add Times"
funcNot      = MkFunction polyNot      Emp (Ty tBool, [Ty tBool]) "Not"
funcPreInt   = MkFunction polyPreInt   Emp (Ty tInt,  [Ty tBool, Ty tInt]) "Pre Int"
funcTimes    = MkFunction polyTimes    Emp (Ty tInt,  [Ty tInt, Ty tInt]) "Times"
funcAnd      = MkFunction polyAnd      Emp (Ty tBool, [Ty tBool, Ty tBool]) "And"
-- ****************************************

-- 1 forall
-- ****************************************
funcPair    = MkFunction polyPair    (Ty var0 :# Emp) (Ty $ typeOf (Zy, Zy), [Ty var0]) "Pair"
funcApp     = MkFunction polyApp     (Ty var0 :# Emp) (Ty var0,  [fun (Ty var0) (Ty var0), Ty var0]) "Apply"
funcReverse = MkFunction polyReverse (Ty var0 :# Emp) (Ty $ typeOf [Zy],  [Ty $ typeOf [Zy]]) "Reverse"
-- ****************************************

-- 2 forall
-- ****************************************
funcGroup = MkFunction polyGroup (Ty var0 :# Ty var1 :# Emp)
    (Ty $ typeOf (Zy, Sy Zy), [Ty var0, Ty var1]) "Group"
-- funcFoldr = MkFunction polyFoldr (Ty var0 :# Ty var1 :# Emp)
--     (Ty var1, [fun3 (Ty var0) (Ty var1) (Ty var1), Ty var1 , Ty $ typeOf [Zy]]) "Foldr"
funcMap   = MkFunction polyMap (Ty var0 :# Ty var1 :# Emp)
    (Ty $ typeOf [Sy Zy], [fun (Ty var0) (Ty var1), Ty $ typeOf [Zy]]) "Map"
-- ****************************************


