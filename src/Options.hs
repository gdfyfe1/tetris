{-|
Module      : Options
Description : Options State.
Copyright   : (c) Graham Fyfe, 2018

This module describes the state where the user can change certain options.
-}

module Options where

import qualified Data.Sequence                    as Seq
import           Graphics.Gloss
import           Graphics.Gloss.Interface.IO.Game

import           GameState
import           PicRow
import           Settings


update :: Float -> OptionsState -> IO GameState
update _ os = return $ OptionsState os

render :: OptionsState -> IO Picture
render os = return $
                   pictures [rBG,
                             rHUD os]
    where rBG = color (makeColorI 173 244 250 255) $
                          rectangleSolid (fromIntegral width) (fromIntegral height)

-- | Combines the options and the red triangle into a picture.
rHUD :: OptionsState -> Picture
rHUD os = let pr = rOpts os
          in translate (xOff + 50) (yOff - 100) $
                             pictures [rPicRow pr,
                                       yPiece rTri pr (selOpt os)]

-- | Describes the options list as PicRow.
rOpts ::  OptionsState -> PicRow
rOpts os = foldl rOpt
                 (PicRow [] 150 2)
                 (optOptions os)

-- | Describes a single option.
rOpt :: PicRow -> ListOpt -> PicRow
rOpt pr (OptBool b  str) = addPic (text $ str ++ "    " ++ show b) pr
rOpt pr (OptInt  ir str) = addPic (text $ str ++ "    " ++ show (fromIR ir)) pr

tsOff = 80
-- | Describes the red selection triangle.
rTri = translate (-100) 0 $ color red $ polygon [(0, 0),
                                                 (0, tsOff),
                                                 (0 + tsOff, tsOff / 2)]

-- Handle events
handleEvents :: Event -> OptionsState -> IO GameState
handleEvents (EventKey (SpecialKey KeyUp)    Down _ _) os = return $ OptionsState $ changeOpt (-1) os
handleEvents (EventKey (SpecialKey KeyDown)  Down _ _) os = return $ OptionsState $ changeOpt   1 os
handleEvents (EventKey (SpecialKey KeyLeft)  Down _ _) os = return $ OptionsState $ adjustOpt left os
handleEvents (EventKey (SpecialKey KeyRight) Down _ _) os = return $ OptionsState $ adjustOpt right os
handleEvents (EventKey (SpecialKey KeySpace) Down _ _) os = return $ OptionsState $ adjustOpt right os
handleEvents (EventKey (SpecialKey KeyEsc)   Down _ _) os = return $ optionsToMenu os

handleEvents _ os = return $ OptionsState os

-- | Changes the current option either up or down.
changeOpt :: Int -> OptionsState -> OptionsState
changeOpt n os = os {selOpt = ((+ n) . selOpt $ os) `mod` (Seq.length . optOptions $ os)}

-- | Adjusts a specific option using the passed function.
adjustOpt :: (ListOpt -> ListOpt) -> OptionsState -> OptionsState
adjustOpt f os = os { optOptions = Seq.adjust' f (selOpt os) (optOptions os)}

enterMenu :: MenuState -> Options -> GameState
enterMenu ms op = MenuState $ ms { svdOpts = op}





