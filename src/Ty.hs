{-# LANGUAGE DataKinds        #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs            #-}
{-# LANGUAGE PolyKinds        #-}
{-# LANGUAGE RankNTypes       #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeInType       #-}
{-# OPTIONS_HADDOCK prune#-}

{-|
Module      : Ty
Description : Type taxation with representation.
Copyright   : (c) Graham Fyfe, 2018

This module contains most of the type related logic that powers the games generality
and inference.
-}

module Ty where

import           Control.Applicative
import           Control.Monad
import           Data.Kind
import           Data.Maybe
import           System.Random
import           Type.Reflection

import           Natty

-- | A mapping from one type (usually a variable) to another.
type TypeMap = (Ty Type, Ty Type)

-- | A version of TypeRep indexed not by type but by kind.
data Ty k where
    Ty :: forall a. TypeRep (a :: k) -> Ty k

class TypeUpdate a where
    update     :: TypeMap   -> a -> a
    updateMult :: [TypeMap] -> a -> a
    updateMult tms x = foldr update x tms

instance TypeUpdate (Ty k) where
    update (Ty a, b) (Ty t) = case eqTypeRep t a of
            Just HRefl -> b
            Nothing    -> fromMaybe (Ty t) (inner (Ty a, b) (Ty t))

    updateMult tms (Ty t) = case foo tms (Ty t) of
            Just t' -> t'
            Nothing -> fromMaybe (Ty t) (innerMult tms (Ty t))
        where foo :: [TypeMap] -> Ty k -> Maybe (Ty k)
              foo ((Ty a, b) : ts) (Ty t) = case eqTypeRep t a of
                Just HRefl -> Just b
                Nothing    -> foo ts (Ty t)
              foo [] t = Nothing

instance TypeUpdate a => TypeUpdate [a] where
    update tm = map (update tm)
    updateMult tms = map (updateMult tms)

instance Eq (Ty k) where
  Ty a == Ty b = case eqTypeRep a b of
    Nothing ->  False
    Just _  ->  True

instance Ord (Ty k) where
    compare (Ty x) (Ty y) = compare (SomeTypeRep x) (SomeTypeRep y)

instance Show (Ty k) where
    show (Ty t) = case t of
        App ls a
            | Just HRefl <- eqTypeRep (typeRep @[]) ls
                -> concat [ "[",
                            show $ Ty a,
                            "]" ]
        App a b
            | Just HRefl <- eqTypeRep (typeRep @Natty) a
                -> show $ repToNat b
        App (App comma a) b
            | Just HRefl <- eqTypeRep (typeRep @(,)) comma
                -> concat [ "(",
                            show $ Ty a,
                            ", ",
                            show $ Ty b,
                            ")" ]
        App (App (App comma a) b) c
            | Just HRefl <- eqTypeRep (typeRep @(,,)) comma
                -> concat [ "(",
                            show $ Ty a,
                            ", ",
                            show $ Ty b,
                            ", ",
                            show $ Ty c,
                            ")" ]
        App (App (App (App comma a) b) c) d
            | Just HRefl <- eqTypeRep (typeRep @(,,,)) comma
                -> concat [ "(",
                            show $ Ty a,
                            ", ",
                            show $ Ty b,
                            ", ",
                            show $ Ty c,
                            ", ",
                            show $ Ty d,
                            ")" ]
        Fun a b -> concat [ show $ Ty a,
                            "->",
                            show $ Ty b ]
        _ -> show t


-- | Takes a TypeRep and tries to convert it to a Ty of kind Type
toType :: forall (k :: Type) (a :: k). TypeRep a -> Maybe (Ty Type)
toType x = do
    HRefl <- eqTypeRep (typeRepKind x) star
    return (Ty x)

-- | inner acts is used to split types so that they can be checked and updated
inner :: TypeMap -> Ty k -> Maybe (Ty k)
inner tm (Ty (Fun s t)) = do
    s' <- toType s
    t' <- toType t
    return $ fun (update tm s') (update tm t')
inner tm (Ty (App f s)) =
  case (update tm (Ty f), update tm (Ty s)) of
    (Ty f', Ty s') -> return . Ty $ App f' s'
inner _ _ = Nothing

-- | inner but with a list of type maps
innerMult :: [TypeMap] -> Ty k  -> Maybe (Ty k)
innerMult tms (Ty (Fun s t)) = do
    s' <- toType s
    t' <- toType t
    return $ fun (updateMult tms s') (updateMult tms t')
innerMult tms (Ty (App f s)) =
  case (updateMult tms (Ty f), updateMult tms (Ty s)) of
    (Ty f', Ty s') -> return . Ty $ App f' s'
innerMult _ _ = Nothing

-- | Takes a type and breaks it into a list of any inner types it may have.
-- | Useful for function where the structure of the types doesn't matter.
asList :: Ty Type -> [Ty Type]
asList (Ty tr) = case tr of
    App ls a
        | Just HRefl <- eqTypeRep (typeRep @[]) ls
            -> [Ty a]
    App (App comma a) b
        | Just HRefl <- eqTypeRep (typeRep @(,)) comma
            -> (asList $ Ty a) ++ (asList $ Ty b)
    App (App (App comma a) b) c
        | Just HRefl <- eqTypeRep (typeRep @(,,)) comma
            -> (asList $ Ty a) ++ (asList $ Ty b) ++
               (asList $ Ty c)
    App (App (App (App comma a) b) c) d
         | Just HRefl <- eqTypeRep (typeRep @(,,,)) comma
             -> (asList $ Ty a) ++ (asList $ Ty b) ++
                (asList $ Ty c) ++ (asList $ Ty d)
    Fun s t -> (asList $ fromMaybe (error "s") (toType s)) ++
               (asList $ fromMaybe (error "t") (toType t))
    _ -> [Ty tr]


-- | Takes a list of types and returns a typemap
-- | from the vars in the list to a random primitives
randTM :: [Ty Type] -> IO [TypeMap]
randTM ts = foldM foo [] ts
    where foo tms t = do
                    let t' = updateMult tms t
                    if isNatty t'
                    then do
                        p <- randomPrim
                        return $ (t', p) : tms
                    else return tms

-- | Applies the random tm to a type
varToRand :: Ty Type -> IO (Ty Type)
varToRand ty = do
            tm <- randTM $ asList ty
            return $ updateMult tm ty

-- | Checks if a type is a variable
isNatty :: Ty Type -> Bool
isNatty (Ty t) = typeRepTyCon t == con
    where con = typeRepTyCon $ typeOf Zy

-- | Converts a natty into a type rep with kind Nat
nat :: Natty n -> Ty Nat
nat n = Ty (natToRep n)

-- | Converts a type rep with kind Nat into type rep with kind Type
natty :: Ty Nat -> Ty Type
natty (Ty n) = Ty (App (typeRep @Natty) n)

-- | Composes the above functions
nattynat :: Natty n -> Ty Type
nattynat = natty . nat

-- | Adds two nattys and returns the type of the addition
addy :: Natty m -> Natty n -> Ty Type
addy m n = nattynat $ nyAdd m n

-- | Converts a Natty to a type rep
natToRep :: Natty n -> TypeRep n
natToRep Zy     = typeRep
natToRep (Sy n) = withTypeable (natToRep n) typeRep

-- | Converts a type rep to a natty
repToNat :: TypeRep n -> Natty n
repToNat (App s n)
  | Just HRefl <- eqTypeRep s (typeRep @'Suc) = Sy (repToNat n)
  | otherwise = error "imp possible"
repToNat z
  | Just HRefl <- eqTypeRep z (typeRep @'Zero) = Zy
  | otherwise = error "impposs ible"

-- up type
-- takes a current var count and a type
-- raises the type to the var count

nextVar ::  Ty Type -> Ty Type
nextVar = upTypeN (Sy Zy)

--should be natty
upType :: Ty Type -> Ty Type -> Ty Type
upType (Ty n) t = case n of
    App a b
       | Just HRefl <- eqTypeRep (typeRep @Natty) a
           -> upTypeN (repToNat b) t
    _      -> t

upTypeN :: Natty n -> Ty Type -> Ty Type
upTypeN x t = updateMult (upTM x t) t

upTM :: Natty n -> Ty Type -> [TypeMap]
upTM x t = foldl step [] (asList t)
    where step tms (Ty t)=
            case t of
                   App a b
                      | Just HRefl <- eqTypeRep (typeRep @Natty) a
                          -> (natty $ Ty b, addy x (repToNat b)) : tms
                   _      ->                                       tms

-- floor type
floorType :: Ty Type -> Ty Type
floorType t = updateMult (floorTM t) t

floorTM :: Ty Type -> [TypeMap]
floorTM t = fst $ foldl step ([], Ty var0) (asList t)
    where step :: ([TypeMap], Ty Type) ->  Ty Type -> ([TypeMap], Ty Type)
          step (tms, n) (Ty t) =
            case ((hasMap (Ty t) tms), t) of
                   (False, App a b)
                      | Just HRefl <- eqTypeRep (typeRep @Natty) a
                          -> ((natty $ Ty b, n): tms ,
                              upTypeN (Sy Zy) n)
                   _      -> (tms, n)

hasMap :: Ty Type -> [TypeMap] ->  Bool
hasMap t = any (\(x, _) -> x == t)



-------------------------------TYPES----------------------------

star :: TypeRep Type
star = typeRep

tInt :: TypeRep Int
tInt = typeRep

tBool :: TypeRep Bool
tBool = typeRep

tChar :: TypeRep Char
tChar = typeRep

var0 = typeOf Zy
var1 = typeOf $ Sy Zy
var2 = typeOf $ Sy $ Sy Zy
var3 = typeOf $ Sy $ Sy $ Sy Zy

fun :: Ty Type -> Ty Type-> Ty Type
fun (Ty x) (Ty y) = Ty $ Fun x y
fun3 :: Ty Type -> Ty Type -> Ty Type -> Ty Type
fun3 (Ty x) (Ty y) (Ty z) = Ty $ Fun x $ Fun y z

randomPrim :: IO (Ty Type)
randomPrim = sel <$> randomRIO (0, 2::Int)
    where sel 0 = Ty tInt
          sel 1 = Ty tBool
          sel _ = Ty tChar











