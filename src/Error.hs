{-# LANGUAGE FlexibleContexts #-}

{-|
Module      : Error
Description : Error Handling.
Copyright   : (c) Graham Fyfe, 2018

This module provides support for error handling.
 In particular the combination of Either and IO.
-}

module Error where

import           Control.Monad.Except
import           Control.Monad.Trans.Except

type EitherIO = ExceptT Error IO

type Message = String

-- | General error data type
data Error = Error Message |
             ErrorVarUnbound    Message  |
             ErrorMissingArg    Message  |
             ErrorExtraArg     [Message] |
             ErrorAppFail       Message  |
             ErrorOntoData      Message  |
             ErrorTreeFormat    Message  |
             ErrorOntoFunction  Message  |
             ErrorNoFunction    Message  |
             ErrorDroppedEmpty  Message            deriving Show

-- | Exits the program on Left value.
handleIOErr :: EitherIO a -> IO a
handleIOErr e = do
    res <- runExceptT e
    case res of
        Left  err -> error $ show err
        Right x   -> return x

-- | Exits the program on Left value.
handleErr :: Either Error a -> a
handleErr e = case e of
                Left  err -> error $ show err
                Right x   -> x

-- | Lifts Either monad to ExceptT
liftEither :: Monad m => Either a b -> ExceptT a m b
liftEither = either throwError return
