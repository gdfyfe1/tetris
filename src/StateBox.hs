{-|
Module      : StateBox
Description : The general content of the gameboard.
Copyright   : (c) Graham Fyfe, 2018

This module contains the general unit which the games trees holds as well
functions for updating abnd performing inference with them.
-}

module StateBox where

import           Data.Kind

import           Application
import           Error
import           Infer
import           Ty

-- | The contents can either be data, a hole or a function.
data Cont = Data Data | Hole (Ty Type) | Func Function deriving (Show, Eq)

instance TypeUpdate Cont where
    update tm (Hole t)  = Hole $ update tm t
    update tm (Func f ) = Func $ update tm f
    update tm  x        = x
    updateMult tms (Hole t)  = Hole $ updateMult tms t
    updateMult tms (Func f ) = Func $ updateMult tms f
    updateMult tms  x        = x

-- | Statebox is used entirely to wrap cont in the context that it can be rejected.
data StateBox = Cont { cont :: Cont, isRej :: Bool}  deriving (Show, Eq)

instance TypeUpdate StateBox where
    update tm (Cont c b) = Cont (update tm c) b
    updateMult tms (Cont c b) = Cont (updateMult tms c) b

-- | Primer to function application which ensures the passed cont is a function.
runFunction :: Cont -> [Cont] -> Either Error Cont
runFunction (Func fun) dat = runFunction' fun dat
runFunction  c        _    = Left $ ErrorTreeFormat  $ "Expected function, instead got " ++ show c

-- | Takes a function and a list of potential data and produces the appropriate output.
runFunction' :: Function -> [Cont] -> Either Error Cont
runFunction' fun dat = do
                        res <- run fun (getDyns dat)
                        return $ Data $ MkData res (funType fun) Nothing
    where getDyns (Data (MkData dyn _ _):ds) = dyn : getDyns ds
          getDyns  []                        = []


-- | A primer to type inference which ensures that of the two compared
-- | Conts, one in a Hole.
getTypeMap  :: Cont -> Cont -> Either Error (Maybe [TypeMap])
getTypeMap (Data    dat) _                  = Left $ ErrorOntoData     $ "Tried to drop piece onto " ++ show dat
getTypeMap (Func    fun) _                  = Left $ ErrorOntoFunction $ "Tried to drop piece onto " ++ show fun
getTypeMap (Hole t) (Hole t')               = Right $ infer t t'
getTypeMap (Hole t) (Func fun)              = Right $ infer t (funType fun)
getTypeMap (Hole t) (Data  (MkData _ t' _)) = Right $ infer t t'

-- | A show function which takes a render state as an argument.
-- | If true functions will show names rather than types.
showSB :: Bool -> StateBox ->  String
showSB b sb = case cont sb of
       (Hole t)
           ->  show t
       (Data (MkData d _ Nothing))
           -> showDyn d
       (Data (MkData d _ (Just str)))
            | b         -> str
            | otherwise -> showDyn d
       (Func (MkFunction _ _ (t, _) str))
            | b         -> str
            | otherwise -> show t

setRej :: StateBox -> Bool -> StateBox
setRej (Cont c _) = Cont c

convRej :: Bool -> Cont -> StateBox
convRej b x = Cont x b

getType :: StateBox -> Ty Type
getType sb = case cont sb of
              (Data d) -> dataType d
              (Func f) -> funType  f
              (Hole t) ->          t

isData :: StateBox -> Bool
isData sb = case cont sb of
               (Data _) -> True
               _        -> False

isHole :: StateBox -> Bool
isHole sb  = case cont sb of
               (Hole _) -> True
               _        -> False

isFunc :: StateBox -> Bool
isFunc sb  = case cont sb of
               (Func _) -> True
               _        -> False

isReady :: StateBox -> Bool
isReady sb = isHole sb && not (isRej sb)