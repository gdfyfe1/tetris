{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE FlexibleContexts   #-}
{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE GADTs              #-}
{-# LANGUAGE KindSignatures     #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies       #-}
{-# LANGUAGE TypeInType         #-}

{-|
Module      : Natty
Description : Natural number used for variables.
Copyright   : (c) Graham Fyfe, 2018

This module describes natural numbers and the Natty singletons.
It also ways to manipulate the types.
-}

module Natty where

import           Data.Kind
import           Type.Reflection
import Data.Char (chr)

-- | Peano numerals
data Nat :: * where
    Zero :: Nat
    Suc  :: Nat -> Nat
    deriving (Typeable)

instance Show Nat where
    show = varName . natAsInt

-- | Singleton for the natural numbers.
data Natty :: Nat -> * where
    Zy :: Natty Zero
    Sy :: Natty n -> Natty (Suc n)

type family   NAdd (a :: Nat) (b :: Nat) :: Nat
type instance NAdd n  Zero    = n
type instance NAdd n (Suc m) = Suc (NAdd n m)

instance Show (Natty n) where
    show = varName . nattyAsInt


natAsInt :: Nat -> Int
natAsInt Zero    = 0
natAsInt (Suc n) = 1 + natAsInt n

intAsNat :: Int -> Nat
intAsNat i | i == 0 = Zero
           | otherwise = Suc $ intAsNat $ i - 1

-- | Converts an Int to variable name: a b .. ba
varName :: Int -> String
varName n = map (chr . (+97)) (reverse $ doo n)
    where doo n | n < 26 = [n]
                | otherwise = n `mod` 26 : doo (n `div` 26)


nattyAsInt :: Natty n -> Int
nattyAsInt Zy     = 0
nattyAsInt (Sy n) = 1 + nattyAsInt n

nAdd :: Nat -> Nat -> Nat
nAdd x  Zero   = x
nAdd x (Suc y) = Suc $ nAdd x y

nyAdd ::  Natty n -> Natty m -> Natty (NAdd n m)
nyAdd x  Zy    = x
nyAdd x (Sy y) = Sy $ nyAdd x y



-- deriving instance Show n => Show (Natty n)

