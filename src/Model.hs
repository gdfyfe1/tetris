{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE UndecidableInstances  #-}

{-|
Module      : Model
Description : Base access point of the model.
Copyright   : (c) Graham Fyfe, 2018

This module is the main connection point between the game elements and the underlying systems.
Most of the functions follow a single chain of when a piece hits the board.
-}

module Model where

import qualified Data.Sequence              as Seq

import           Control.Monad.IO.Class
import           Control.Monad.Trans.Except
import           Data.Dynamic
import           Data.Kind
import           Data.Maybe
import           Type.Reflection

import           Application
import           Error
import           Generator
import           Infer
import           Piece
import           RepoFunction
import           StateBox
import           Ty
import qualified ZipList                    as ZL
import           Zipper

type Score = Int

-- | Updates the board with a piece. Handles the different power-ups.
updatePiece :: ZL.ZipList StateBox ->
                 Piece -> EitherIO (ZL.ZipList StateBox, Maybe Piece, Score)
updatePiece zl (Reg pc)     = updateNP zl (Just pc)
updatePiece zl (Power Fill) = do
                            pc <- liftIO $ fillPiece zl
                            updateNP zl pc
updatePiece zl (Power Yank) = do
        case yank <$> (ZL.curr zl) of
             Nothing      -> throwE $ Error "impossible hopefully"
             Just (z, pc) -> return (ZL.setZip zl (Just $ setZipperRej False z),
                                     Reg <$> pc,
                                     0)
updatePiece zl (Power Kill) = undefined

-- | Updates the board with a piece that is now known to not be a power-up.
-- | Handles the situation where the piece is Nothing.
updateNP :: ZL.ZipList StateBox -> Maybe (Tree StateBox) ->
    EitherIO (ZL.ZipList StateBox, Maybe Piece, Score)
updateNP zl Nothing   = do
    pc'        <-              genPiece zl
    return (zl, Just pc', 0)
updateNP zl (Just pc) = do
    zip        <- liftEither $ updateMZip (ZL.curr zl) pc
    (zip', s)  <- liftEither $ updateData zip
    let zl'    =               ZL.setZip zl zip'
    pc'        <-              genPiece zl'
    return (zl', Just pc', s)

-- | Specific part of the game board has now been focused on.
-- | Handles the situation that the zipper may be empty.
updateMZip :: Maybe (Zipper StateBox) ->
                   Tree StateBox -> Either Error (Zipper StateBox)
updateMZip (Just old) new = updateZip old new
updateMZip Nothing    new = if isFunc . currTree $ new
                            then return  . asZip $ new
                            else Left $ ErrorTreeFormat $ show new ++ " should be a function."


-- | Non empty tree + non nothing -> inference can occur
-- | Inference happens elsewhere, this function deals with the result.
updateZip :: Zipper StateBox ->
              Tree StateBox -> Either Error (Zipper StateBox)
updateZip old new = do
            tm  <- getTypeMap (cont . currZip $ old) (cont . currTree $ new)
            case tm of
                (Just tm') ->  return $ mapZ (updateMult tm') (insert old new)
                Nothing    ->  return $ addRej old new

-- | Checks if application needs to be done.
-- | Does so if required.
updateData :: Zipper StateBox -> Either Error (Maybe (Zipper StateBox), Score)
updateData z = foo z 0
    where foo zip i | isSingleData zip = return (Nothing, i)
                    | isReadyForApp zip = do
                      res <- runFunction
                              (cont . currZip . up $ zip)
                              (map cont (generation zip))
                      foo (addSingle (up zip) (convRej False res)) ((i + 5) * 5)
                    | otherwise = return (Just zip, i)

-- | Is the current focus of the zipper a function which is ready for application.
isReadyForApp :: Zipper StateBox -> Bool
isReadyForApp zip = (isFunc . currZip . up $ zip) &&
                     all check (generation zip)
    where check sb = isData sb && not (isRej sb)

-- | Is the current focus of the zipper some data on its own.
isSingleData :: Zipper StateBox -> Bool
isSingleData zip = isData (currZip zip) && depthZ zip == 0

-- | Sets the current focus of the zipper's rejection state.
setZipperRej :: Bool -> Zipper StateBox ->  Zipper StateBox
setZipperRej b (Leaf x, bs)    = (Leaf (setRej x b),    bs)
setZipperRej b (Node x xs, bs) = (Node (setRej x b) xs, bs)

-- | Adds a to a zipper and sets the addition to be rejected.
addRej :: Zipper StateBox -> Tree StateBox -> Zipper StateBox
addRej old new = setZipperRej True $ addTo old new

-- | Moves the current focus of game board, based on the falling piece.
moveHole :: Bool -> ZL.ZipList StateBox -> (Bool, StateBox -> Bool) -> ZL.ZipList StateBox
moveHole right zl (nt, pre) = let mv = if right then ZL.nextPre else ZL.backPre
                              in fromMaybe
                                 (handleErr $ Left $ ErrorTreeFormat $ show (nt))
                                 (mv nt pre zl)



test0 :: Tree Cont
test0 =  Node  (Func funcPreInt)
               [Leaf $ Hole (Ty tBool),
                Leaf $ Hole (Ty tInt)]

testC0 = fmap (convRej False) test0





















