{-|
Module      : Main
Description : Main Program Entry Point.
Copyright   : (c) Graham Fyfe, 2018

This module is the main entry point of the program.
Most of the code here originates from the Gloss game engine.
-}

module Main where

import           Graphics.Gloss
import           Graphics.Gloss.Interface.IO.Game

import           GameState

import qualified Menu
import qualified Options
import qualified Play

window :: Display
window = InWindow "Hindley-Milner Tetris" (width, height) (10, 10)

background :: Color
background = white

fps :: Int
fps = 60

-- | Base render function. Split by state.
render :: GameState -> IO Picture
render (MenuState ms)    = Menu.render ms
render (PlayState ps)    = Play.render ps
render (OptionsState os) = Options.render os

-- | Base event function. Split by state.
handleEvents :: Event -> GameState -> IO GameState
handleEvents e (MenuState ms)    = Menu.handleEvents e ms
handleEvents e (PlayState ps)    = Play.handleEvents e ps
handleEvents e (OptionsState os) = Options.handleEvents e os

-- | Base update function. Split by state.
update :: Float -> GameState -> IO GameState
update dt (MenuState ms)    = Menu.update dt ms
update dt (PlayState ps)    = Play.update dt ps
update dt (OptionsState os) = Options.update dt os


main :: IO ()
main = playIO window background fps (MenuState initialMenuState) render handleEvents update


