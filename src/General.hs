{-|
Module      : General
Description : General functions.
Copyright   : (c) Graham Fyfe, 2018

This module describes some general functions that are used in multiple places.
-}

module General where

import           System.Random

-- | Chooses a random element from a list. Fails on empty list.
choice :: [a] -> IO a
choice xs = (xs !!) <$> randomRIO (0, length xs - 1)

-- | A map function with an incrementing Int.
imap :: (Int -> a -> b) -> [a] -> [b]
imap f = go 0
  where
    go i (x:xs) = f i x : go (i + 1) xs
    go _ _      = []

-- | Dodgy Haskell thing.
com2 = (.) . (.)
