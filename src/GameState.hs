{-|
Module      : GameState
Description : Definition of a game's state.
Copyright   : (c) Graham Fyfe, 2018

This module contains the data types that describe the games different states.
It also contains methods for moving between them.
-}

module GameState where

import           Data.Kind
import           Data.Maybe    (fromMaybe)
import qualified Data.Sequence as Seq


import           Model
import           Natty
import           PicRow
import           Piece
import           Settings
import           StateBox
import           Ty
import           ZipList
import           Zipper

width, height :: Int
width  = 1280
height = 720

xOff, yOff :: Float
xOff =  fromIntegral width  / (-2)
yOff =  fromIntegral height / 2

-- | These three constructors define the three states of the game.
data GameState = MenuState MenuState | PlayState PlayState | OptionsState OptionsState

data MenuState = MState { selMenu     :: Int,                                 -- ^ Currently selected menu option.
                          menuOptions :: [(MenuState -> GameState, String)],  -- ^ Functions performed by the menu options.
                          isFinished  :: Bool,                                -- ^ Game over?
                          svdPS       :: Maybe PlayState,                     -- ^ Saved play state for continue.
                          svdOpts     :: Options                              -- ^ Saved options.
                         }
data PlayState = PState { forest      :: ZipList StateBox,  -- ^ Game board.
                          piece       :: Maybe Piece,       -- ^ Falling piece.
                          pieceX      :: Float,             -- ^ Piece's x position.
                          pieceY      :: Int,               -- ^ Piece's relative y position.
                          dropRate    :: Float,             -- ^ How fast the piece drops.
                          optsP       :: Options,           -- ^ Game's options.
                          rMode       :: Bool,              -- ^ Render mode of the game.
                          saved       :: Maybe Piece,       -- ^ Saved piece.
                          score       :: Int,               -- ^ Current score.
                          cachedBoard :: Maybe PicRow,      -- ^ Cached board for efficiency.
                          currVar     :: Ty Type            -- ^ Current Variable.
                          }
data OptionsState = OState { selOpt     :: Int,             -- ^ Currently selected option.
                             optOptions :: Seq.Seq ListOpt, -- ^ Available options.
                             svdMS      :: MenuState        -- ^ Saved menu state.
                           }
initialMenuState :: MenuState
initialMenuState = MState 0 [(menuToPlay True,  "New Game"),
                             (menuToPlay False, "Continue"),
                             (menuToOptions,    "Options"),
                             (exitGame,         "Exit")]
                          False
                          Nothing
                          stdOption

initialPlayState :: PlayState
initialPlayState = PState
                         initForest
                         initPiece
                         initX initY initDrop
                         stdOption
                         initRenderMode
                         initSaved
                         initScore
                         initPic
                         initVar
    where initForest     = zipList [Nothing, Nothing, Nothing, Nothing]
          initPiece      = Just $ Reg testC0
          initX          = 1200
          initY          = 0
          initDrop       = 5
          initRenderMode = True
          initSaved      = Nothing
          initScore      = 0
          initPic        = Nothing
          initVar        = Ty var0

initialOptionState :: OptionsState
initialOptionState = OState 0 initOptions initialMenuState
    where initOptions = insertOptions  stdOption


menuToOptions :: MenuState -> GameState
menuToOptions ms = OptionsState $ OState 0 (insertOptions . svdOpts $ ms) ms

menuToPlay ::  Bool -> MenuState -> GameState
menuToPlay b ms | b         = PlayState $ setOpt $ initialPlayState
                | otherwise = let newPS = fromMaybe initialPlayState (svdPS ms)
                              in PlayState $ setOpt newPS
    where setOpt ps' = ps' {optsP = svdOpts ms}

optionsToMenu :: OptionsState -> GameState
optionsToMenu os = MenuState $ ms { svdOpts = extractOptions . optOptions $ os}
    where ms = svdMS os

playToMenu :: PlayState -> GameState
playToMenu ps = MenuState $ initialMenuState { svdPS = Just ps, svdOpts = optsP ps}

--Menu functions
--These should be in the menu class but are here to avoid circular imports
--Considor redesign
----------------------------------------------------------------------

exitGame :: MenuState -> GameState
exitGame ms = MenuState (ms {isFinished = True})

-----------------------------------------------------------------


















