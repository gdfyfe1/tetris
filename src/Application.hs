{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE KindSignatures      #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StandaloneDeriving  #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE ViewPatterns        #-}

{-|
Module      : Application
Description : Generic Function Application
Copyright   : (c) Graham Fyfe, 2018

This module provides functionality for general function application using dynamics.
Polymorphic functions are supported.
-}

module Application where

import           Data.Dynamic
import           Data.Kind
import           Data.List       (intercalate)
import           Data.Maybe
import           Type.Reflection

import           Error
import           Natty
import           Ty

-- | A function's name.
type Name = String

-- | Data nodes of the tree.
data Data where
    MkData :: Dynamic -> Ty Type -> Maybe Name -> Data    deriving Show

-- shallow equality
instance Eq Data where
    (MkData _ s x) == (MkData _ t y) = x == y && s == t

-- | Functions nodes of the tree.
data Function where
    MkFunction :: Poly (n :: Nat) -> Vec n (Ty Type) -> (Ty Type, [Ty Type]) -> Name ->  Function

instance TypeUpdate Function where
    update tm (MkFunction p vec (t, ts) s) =
        MkFunction p
                  (fmap (update tm) vec)
                  (update tm t, map (update tm) ts)
                   s
    updateMult tms (MkFunction p vec (t, ts) s) =
            MkFunction p
                      (fmap (updateMult tms) vec)
                      (updateMult tms t, map (updateMult tms) ts)
                       s

instance Show Function where
    show (MkFunction _ all arg name) = name -- ++ "  Poly: " ++ show all ++  "  Args: " ++ show arg

instance Eq Function where
    (MkFunction _ _ _ x) == (MkFunction _ _ _ y) = x == y

-- | Polymorphic function handler.
data Poly :: Nat -> * where
    Mono :: Dynamic -> Int -> Poly Zero
    All  :: (Ty Type -> Poly n) -> Poly (Suc n)

-- | Nat indexed vectors.
--   Copyright : https://blog.jle.im/entry/fixed-length-vector-types-in-haskell-2015.html
data Vec :: Nat -> * -> * where
    Emp   :: Vec Zero a
    (:#)  :: a -> Vec n a -> Vec (Suc n) a

infixr 5 :#

instance Functor (Vec n) where
    fmap f (x :# xs) = f x :# fmap f xs
    fmap _ Emp       = Emp

deriving instance Show a => Show (Vec n a)
deriving instance Eq a => Eq (Vec n a)

-- | Ensures function is monomorphic and then executes.
run :: Function -> [Dynamic] -> Either Error Dynamic
run f ds = do
            x <- mono f
            exec x ds

-- | Converts a polymorphic function to a monomorphic one.
mono :: Function -> Either Error (Poly Zero)
mono (MkFunction (Mono x i ) _ _ _)                             = return $ Mono x i
mono (MkFunction (All f) (Ty t :# ts) args name )
        | isNatty (Ty t)   = Left $ ErrorVarUnbound $ show t ++ "has not been bound"
        | otherwise   = mono (MkFunction (f (Ty t)) ts args name)

-- | Executes a function with a list of dynamic input.
exec :: Poly Zero -> [Dynamic] -> Either Error Dynamic
exec (Mono f 0)  []      = Right f
exec (Mono f 0)  ds      = Left $ ErrorExtraArg (map show ds)
exec (Mono f i)  []      = Left $ ErrorMissingArg $ "Expected " ++ show i ++ " more argument(s) for function " ++ show f
exec (Mono f i) (d : ds) = do
                        r <- app
                        exec (Mono r (i - 1)) ds
    where app = case dynApply f d of
                Nothing  -> Left $ ErrorAppFail $ "Unable to apply " ++ show d ++ " to " ++ show f
                Just res -> Right res

-- | A more sensible print for dynamic values.
showDyn :: Dynamic -> String
showDyn (Dynamic (App (eqTypeRep (typeRep @[]) -> Just HRefl) ta) vs)
    = concat [ "[",
               intercalate "," (map (showDyn . Dynamic ta) vs),
               "]" ]
showDyn (Dynamic (App (App (eqTypeRep (typeRep @(,)) -> Just HRefl) ta) tb) (va, vb))
    = concat [ "(",
               showDyn (Dynamic ta va),
               ", ",
               showDyn (Dynamic tb vb),
               ")" ]
showDyn (Dynamic (App (App (App
    (eqTypeRep (typeRep @(,,)) -> Just HRefl)
        ta) tb) tc) (va, vb, vc))
    = concat [ "(",
               showDyn (Dynamic ta va),
               ", ",
               showDyn (Dynamic tb vb),
               ", ",
               showDyn (Dynamic tc vc),
               ")" ]
showDyn (Dynamic (App (App (App (App
    (eqTypeRep (typeRep @(,,,)) -> Just HRefl)
        ta) tb) tc) td) (va, vb, vc, vd))
    = concat [ "(",
               showDyn (Dynamic ta va),
               ", ",
               showDyn (Dynamic tb vb),
               ", ",
               showDyn (Dynamic tc vc),
               ", ",
               showDyn (Dynamic td vd),
               ")" ]
showDyn (Dynamic (eqTypeRep (typeRep @Int)  -> Just HRefl) n) = show n
showDyn (Dynamic (eqTypeRep (typeRep @Bool) -> Just HRefl) b) = show b
showDyn (Dynamic (eqTypeRep (typeRep @Char) -> Just HRefl) c) = show c
showDyn (Dynamic tr _) = show tr

-- | Gets the type of some data.
dataType :: Data -> Ty Type
dataType (MkData _ t _) = t

-- | Gets the output type of a function.
funType :: Function -> Ty Type
funType (MkFunction _ _ (t, _) _) = t














