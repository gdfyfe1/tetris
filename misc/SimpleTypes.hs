
import qualified Data.Map as Map

data Var = V String 

data Type 
	= Tvar Var
	| TInt 
	| TBool

type Fun = ([Type], Type)
	
type Subst = Map.Map String Type

infer :: Fun -> Int -> Type -> Fun
infer ([args], res) n t = 

apply :: Subst -> Type -> Type
apply s (TVar n) = case Map.lookup n s of
					Nothing -> Tvar n 
				 	Just t  -> t
apply s t 		 = t					
					

