data Type = TVar   String
          | TLit   TLit
          | TList  Type
          | TTuple [Type]
          | TFun   [Type]
    deriving (Show, Eq)

data TLit = TBool
          | TInt  
    deriving (Show, Eq)
     
data FunLine = FunLine ([Type], Type)
    deriving (Show, Eq)

type TypeMap = (String, Type) 
    
class TypeUpdate a where
  update     :: TypeMap   -> a -> a
  updateMult :: [TypeMap] -> a -> a
  
instance TypeUpdate Type where
  update  (str, newT) (TVar n)| n == str  = newT 
                              | otherwise = TVar n
  update   tm         (TList t)           = TList  $ update tm t 
  update   tm         (TTuple ts)         = TTuple $ update tm ts
  update   tm         (TFun ts)           = TFun   $ update tm ts
  update    _              t              = t
  updateMult tms t = foldr update t tms
                         
instance TypeUpdate a => TypeUpdate [a] where
    update tm ts = map (update tm) ts
    updateMult tms ts = foldr update ts tms    

instance TypeUpdate FunLine where
  update tm (FunLine (t , out)) = FunLine(update tm t , update tm out)
  updateMult tms fun = foldr update fun tms  
    
updateHoleInt :: FunLine -> Int -> Type -> Maybe FunLine
updateHoleInt f@(FunLine (ts , out)) i newT = do
                                          curr <- lookup 
                                          tm <- getVars newT curr
                                          return $ updateMult tm f
    where lookup
           | i >= 0 && i < length ts = Just $ ts !! i
           | otherwise               = Nothing

getVars :: Type -> Type ->  Maybe [TypeMap]
getVars  t         (TVar n)                 =  Just [(n , t)]

getVars (TLit t1)  (TLit t2) | t1 == t2     = Just []
                             | otherwise    = Nothing
getVars  _         (TLit _)                 = Nothing
getVars (TLit _)    _                       = Nothing

getVars (TList t1) (TList t2)               = getVars t1 t2
getVars (TList  _)  _                       = Nothing         
getVars  _         (TList _)                = Nothing

getVars (TTuple (t1:ts1)) (TTuple (t2:ts2)) = do
                                              vars     <- getVars t1 t2 
                                              restVars <-  getVars (TTuple ts1) (TTuple $ updateMult vars ts2)
                                              return $ vars ++ restVars                         
getVars (TTuple [])      (TTuple [])        = Just []
getVars (TTuple (_:_))   (TTuple [])        = Nothing
getVars (TTuple [])      (TTuple (_:_))     = Nothing
getVars (TTuple _)       _                  = Nothing
getVars  _               (TTuple _)         = Nothing

getVars (TFun (t1:ts1)) (TFun (t2:ts2))     = do
                                          vars     <- getVars t1 t2 
                                          restVars <-  getVars (TFun ts1) (TFun $ updateMult vars ts2)
                                          return $ vars ++ restVars                         
getVars (TFun [])      (TFun [])            = Just []
getVars (TFun (_:_))   (TFun [])            = Nothing
getVars (TFun [])      (TFun (_:_))         = Nothing
getVars (TFun _)       _                    = Nothing
getVars  _               (TFun _)           = Nothing
   
test1 :: FunLine
test1 = FunLine([TVar "a" , TLit TBool , TVar "b", TVar "b"] , TVar "b")

test2 :: FunLine
test2 = FunLine  ([TList $ TVar "a" , TList $ TLit TBool , TLit TInt, TVar "b", TVar "a"] , TVar "b")

test3 :: FunLine
test3 = FunLine([ TVar "b", TTuple [TVar "b", TTuple [TVar "b", TLit TBool, TVar "b"], TVar "b"] ], TVar "b")

test4 :: FunLine
test4 = FunLine([ TVar "b", TFun [TVar "b", TTuple [TVar "b", TLit TBool, TVar "b"], TVar "b"] ], TVar "b")





