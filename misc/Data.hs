type Error = String

data Data =  DInt  Int  |
             DBool Bool |
             DListInt  [Int]  |
             DListBool [Bool] |
             DTupleIntBool 
             deriving (Show, Eq)
         
data Fun0 = Fun0 ((Data -> Either Error Int), (Data -> Either Error Bool))
                    (Int -> Bool -> Int)

                                        
data Rec a = Rec {dat :: a
                 ,bar :: (Data -> Either Error a)}
 
foo :: Int -> Bool -> Int 
foo n p = if p then n + 10 else n - 10 
                    
newtype Parser a = MkParser ([Data] -> Either Error (a , [Data]))                   
              
pa1 = MkParser ( \ _  -> Right (1, []) )  
pa2 = MkParser ( \ _  -> Right (True, []) )           

pas = [pa1, pa2]   
  
exec :: Parser a -> [Data] -> Either Error (a , [Data])
exec (MkParser f) ds = f ds
 
tar :: [Data] ->  (Int -> Bool -> Int) -> Either Error Int
tar ds f = do
        (n, qs) <- exec getInt ds
        (m, ws) <- exec getBool qs
        if null ws 
        then Right (f n m)
        else Left "Too much data"

 
-- test0 :: Rec Int
-- test0 = Rec {dat = DInt 7, foo = getInt}
-- test1 :: Rec Bool
-- test1 = Rec {dat = DBool True, foo = getBool}
-- test2 :: Rec [Int]
-- test2 = Rec {dat = DListInt [1, 2, 3] , foo = getListInt}

getInt :: Parser Int
getInt = MkParser ( \ ds -> aux ds)
     where aux ((DInt n):ds) = Right (n, ds)
           aux (d:ds)        = Left ("Exepected int, found: " ++ show d)
           aux []            = Left "Ran out of input"

getBool :: Parser Bool
getBool = MkParser ( \ ds -> aux ds)
     where aux ((DBool b):ds) = Right (b, ds)
           aux (d:ds)         = Left ("Exepected bool, found: " ++ show d)
           aux []             = Left "Ran out of input"
           
-- getInt :: Data -> Either Error Int
-- getInt (DInt x) = Right x
-- getInt       x  = Left "This data is not an int"

-- getBool :: Data -> Either Error Bool
-- getBool (DBool x) = Right x
-- getBool       x  = Left "This data is not a Bool"

getListInt :: Data -> Either Error [Int]
getListInt (DListInt x) = Right x
getListInt           x  = Left "This data is not a list of ints"
         