{-# LANGUAGE FlexibleInstances #-}

module TreeTypes where

--Considerations********
--Do we even need to store the types of functions

import Zipper
import Data
import Data.Maybe (isJust)
import Data.Dynamic

-- ****************
data StateBox = Cont { cont :: Cont,
                       isRej :: Bool }  deriving Show
data Cont = Data Type Dynamic | Hole Type | Fun Type Function deriving Show

data Type = TVar    String
          | TLit    TLit
          | TList   Type
          | TMaybe  Type
          | TTuple  Type Type [Type]
          | TFun    Type      [Type] 
          | TEither Type Type
    deriving (Show, Eq)

data TLit = TBool
          | TInt  
    deriving (Show, Eq)


--------TYPE-UPDATES-------------    
type TypeMap = (String, Type) 
     
class TypeUpdate a where
  update     :: TypeMap   -> a -> a
  updateMult :: [TypeMap] -> a -> a
  
instance TypeUpdate Type where
  update  (str, newT) (TVar n)| n == str  = newT 
                              | otherwise = TVar n
  update   tm         (TList t)           = TList  $ update tm t 
  update   tm         (TTuple t1 t2 ts)   = TTuple (update tm t1) (update tm t2) (update tm ts)
  update   tm         (TFun t1 ts)        = TFun   (update tm t1) (update tm ts)
  update    _              t              = t
  updateMult tms t = foldr update t tms

instance TypeUpdate StateBox where
    update tm (Cont c b)     = Cont (update tm c) b
    updateMult tm (Cont c b) = Cont (updateMult tm c) b
    
--consider creating instance for update function    
instance TypeUpdate Cont where
    update tm (Hole t)   = Hole (update tm t)
    update tm (Fun  t f) = Fun  (update tm t) f --(updateFunction tm f)
    update tm  x         = x
    updateMult tms x     = foldr update x tms
 
instance TypeUpdate a => TypeUpdate [a] where
    update tm ts = map (update tm) ts
    updateMult tms ts = foldr update ts tms    
   
instance TypeUpdate a => TypeUpdate (Zipper a)where
    update tm zip = mapZ (update tm) zip
    updateMult tms zip = mapZ (updateMult tms) zip

-----------------------------------------------------------------------------------------    

    
updateTree :: Zipper StateBox -> Tree StateBox -> Either String (Zipper StateBox)
updateTree x y = do 
            tm <- updateHole ((cont . currZip) x) ((cont . currTree) y)
            return  (up tm)
    where up (Just ms) = updateMult ms (root (insert x y))
          up  Nothing  = root (addTo x (setTreeRej y))

updateHole :: Cont -> Cont -> Either String (Maybe [TypeMap])
updateHole (Data _ _ ) _         = Left "This is not a hole, it is data."
updateHole (Fun  _ _ ) _         = Left "This is not a hole, it is fun."
updateHole (Hole t)  (Hole t')   = Right (infer t t')
updateHole (Hole t)  (Fun  t' f) = Right (infer t t')
updateHole (Hole t)  (Data t' d) = Right (infer t t')

          
infer :: Type -> Type -> Maybe [TypeMap]
infer (TVar n)              t                    = Just [(n, t)] 
infer  t                   (TVar n)              = Just [(n, t)]           
infer (TLit t1)            (TLit t2) | t1 == t2  = Just []
                                     | otherwise = Nothing 
infer (TList t1)           (TList t2)            = infer t1 t2
infer (TMaybe t1)          (TMaybe t2)           = infer t1 t2
infer (TTuple tx1 ty1 ts1) (TTuple tx2 ty2 ts2)  = do
                                               vars1    <- infer tx1 tx2 
                                               vars2    <- infer ty1 (updateMult vars1 ty2)
                                               restVars <- inferMult ts1 (updateMult (vars1 ++ vars2) ts2) --Double pend, probs bad
                                               return $ vars1 ++ vars2 ++ restVars
infer (TFun tx1 ts1)       (TFun tx2 ts2)        = do
                                               vars     <- infer tx1 tx2 
                                               restVars <- inferMult ts1 (updateMult vars ts2)
                                               return $ vars ++ restVars
infer (TEither tx1 ty1)    (TEither tx2 ty2)     = do
                                               vars1    <- infer tx1 tx2 
                                               vars2    <- infer ty1 (updateMult vars1 ty2)
                                               return $ vars1 ++ vars2                                               
infer  t1                   t2                   = Nothing                                              

inferMult :: [Type] -> [Type] -> Maybe[TypeMap]
inferMult [] []                 = Just []
inferMult []  _                 = Nothing
inferMult  _ []                 = Nothing
inferMult (t1':ts1') (t2':ts2') = do 
                                vars'     <- infer     t1'  t2'
                                restVars' <- inferMult ts1' (updateMult vars' ts2')
                                return $ vars' ++ restVars'
                                
--sets the top level of tree as rejected
--For is a type rejection    
setTreeRej :: Tree StateBox -> Tree StateBox
setTreeRej (Leaf x)    = Leaf (setRej x True)
setTreeRej (Node x xs) = Node (setRej x True) xs 
    
--Set the whether a type was rejected                                
setRej :: StateBox -> Bool -> StateBox 
setRej (Cont c _)  b = Cont c b                            
                            
-- **************************************************************************************************
-- ***********************************TESTING********************************************************
-- **************************************************************************************************                                

foo :: Cont -> StateBox
foo x = Cont x False
                                
-- testTree :: Tree Cont
-- testTree =  Node  (Fun (TLit TInt) (exec testFun0)) 
               -- [Node  (Fun (TLit TBool) (exec testFun1))
                   -- [Leaf $ Data (TLit TInt)(DInt 3),
                    -- Leaf $ Hole $ TLit TInt ,
                    -- Leaf $ Hole $ TList $ TVar "A"],
                -- Node  (Fun  (TVar "B")   (exec testFun2)) 
                   -- [Leaf $ Hole $ TVar "A",
                    -- Leaf $ Hole $ TTuple (TLit TInt) (TVar "B") [TVar "A"]], 
                -- Node  (Fun (TLit TBool) (exec testFun3))
                   -- [Leaf $ Hole $ TVar "A",   
                    -- Leaf $ Data (TLit TInt) (DInt 3), 
                    -- Leaf $ Hole $ TVar "D",
                    -- Leaf $ Data (TLit TBool) (DBool True)],
                -- Node  (Fun (TLit TInt)  (exec testFun4)) 
                   -- [Leaf $ Hole $ TLit TInt]]
                   
-- testCont = (testTree, [])

-- tz = mapZ foo testCont

-- tzl = maybe tz id (goTo tz [0, 2])


-- repTree1 = fmap foo (Leaf $ Hole $ TList $ TLit TInt)
-- repTree2 = fmap foo (Leaf $ Data  (TList $ TLit TInt) ())
-- repTree3 = fmap foo (Node (Fun (TList $ TLit TInt) "foo") 
                      -- [Leaf $ Data (TLit TInt)(DInt 3),
                       -- Leaf $ Hole $ TLit TInt ,
                       -- Leaf $ Hole $ TList $ TVar "A"])
 
rep = [("A", TLit TInt), ("B", TLit TBool)]
 
testFun0 :: Int -> Int -> Int -> Int -> Int 
testFun0 a b c d = a + b + c + d

testFun1 :: Int -> Int -> [Bool] -> Bool
testFun1 a b c = if b == 0 then c !! a else c !! b

testFun2 :: a -> (Int, a, b) -> Int 
testFun2 a (b, c, d) = b

testFun3 :: a -> Int -> d -> Bool -> Bool 
testFun3 a b c d = d

testFun4 :: Int -> Int
testFun4 a = a * 2















