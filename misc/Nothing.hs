{-# LANGUAGE FlexibleInstances,
    UndecidableInstances #-}

class Bar a where
    get :: a -> a
    wow :: a -> Int
    
instance Bar Bool where
    get x = not x 
    wow x = 0
    
instance Bar [Bool] where
    get x = x 
    wow x = 1

instance Bar Int where
    get x = x 
    wow x = 2
    
instance Bar [Int] where
    get x = x      
    wow x = 3
    
instance Bar (Bool, Bool) where
    get (x, y) = (y, x)
    wow x = 4
    
class Foo a where
    exec :: a -> a
    
instance Bar a => Foo a where 
    exec x = x
    
instance {-# OVERLAPPING #-}  (Bar a, Foo r) => Foo (a -> r) where
    exec x = x


    
t :: (Bool, Bool) -> Bool
t (x, y) = x && y  
    
q :: c -> d -> c
q x y = x

e :: a -> [a]
e x = [x, x, x]    






-- yo :: Int -> a
-- yo x 

bo :: Int -> a
bo x | x == 0    = 9
     | otherwise = "Nope"



















