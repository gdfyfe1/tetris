{-# Language
    GADTs, KindSignatures, DataKinds, TypeOperators, ScopedTypeVariables
    #-}
    
    
import Type.Reflection

count  = Zero
data Var :: Nat -> * where
    Start :: Var Zero
    Next  :: Var n -> Var (Suc n)
    
data VarStore where
    MkStore :: Var (a :: Nat) -> VarStore
   
data Type where 
    MkType :: TypeRep(a :: *) -> Type 
 
main = do 
    let x = ((snd . splitApps) wah') !! 1
    let y = ((snd . splitApps) cah') !! 1
    let b = test x y 
    putStrLn (show b)
    --(putStrLn . show . typeOf) x
    --(putStrLn . show . typeOf) y
    where jimmy x y = case eqTypeRep (typeOf x) (typeOf y) of 
                          Nothing -> False
                          Just _  -> True
    

-- test :: VarStore -> VarStore -> IO Bool
-- test (MkStore v) (MkStore v') = do 
                    -- let x = typeOf v
                    -- return True
                    -- case eqTypeRep (typeOf v) (typeOf v') of 
                        -- Nothing -> return False
                        -- Just _  -> return True
                    
test :: SomeTypeRep -> SomeTypeRep -> Bool
test v t = v == t 
                    
    
type TypeMap = (Nat, Int) 
                  

infer :: Type -> Type -> IO ()
infer (MkType t) (MkType t') =  do putStrLn "yeah"

add :: Int -> Int -> Int
add x y = x + y

wah' = typeOf (True, add)   
wah  = MkType wah'
cah' = let x = Start in typeOf (True, Start)      
cah  = MkType cah'

  
data Nat where 
    Zero :: Nat
    Suc  :: Nat -> Nat

    
instance Eq Type where
    (MkType x) == (MkType y) = case eqTypeRep x y of 
                                Nothing -> False 
                                Just _  -> True