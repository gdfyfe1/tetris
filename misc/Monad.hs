import Control.Monad (when)

foo :: Bool -> IO Int
foo b = do 
        if b
        then return 99
        else do 
            let x = 3
            return x