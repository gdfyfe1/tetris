{-# Language GADTs #-}

data Nat where 
  Zero :: Nat
  Suc  :: Nat -> Nat

count :: Nat -> Int
count    Zero = 0
count (Suc n) = 1 + count n

data List a where
  Emp  :: List a
  Cons :: a -> List a -> List a
  
list :: List Int  
list = Cons 2 (Cons 5 (Cons 13 Emp))

foldAdd :: List Int -> Int
foldAdd Emp = 0
foldAdd (Cons x y) = x + foldAdd y