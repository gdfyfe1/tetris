{-# LANGUAGE GADTs, ViewPatterns   #-}
    
data Data =   DInt Int               
            | DBool Bool             
            | DListInt [Int]
            | DListBool [Bool]            
            | DPair Data Data        
            | DTriple Data Data Data   deriving (Show)
data Error = TypeError String Data | MissingArg | ExtraArgs | PolyFunction deriving (Show)
  
class Arg a where 
    getArg :: Data -> Either Error a  
    wrap   :: a -> Data
    
--Basic Literals    
instance Arg Int where
    getArg (DInt x) = Right x
    getArg       x  = Left $ TypeError "DInt" x
    wrap         x  = DInt x
instance Arg Bool where
    getArg (DBool x) = Right x
    getArg        x  = Left $ TypeError "DBool" x
    wrap          x  = DBool x
 
-- funInt :: Function Int
-- funInt = Function help
    -- where help (DInt x : xs) = (Right x, xs)
          -- help             _ = (Left PolyFunction, [])

foo :: Int -> Int -> Int 
foo x y = x + y

bar :: (a -> b) -> Int
bar y = 2

lift :: (a -> b) -> Int 
lift f = 4

data Function a where
    App :: (Arg a) => (a -> b) -> Function (a -> b)
    Res ::                   a -> Function a 
    
exec :: Function a -> [Data] -> Either Error a
exec (App a) (x : xs) = do 
                        x' <- getArg x
                        Left PolyFunction
exec (Res a)    _     = Right a
