{-# LANGUAGE FlexibleInstances,
             UndecidableInstances,
             TypeSynonymInstances, 
             IncoherentInstances #-}

module Data where

import Control.Monad (unless)   

type Func = [Data] -> Either Error Data

data Data =   DInt Int               
            | DBool Bool             
            | DListInt [Int]
            | DListBool [Bool]            
            | DPair Data Data        
            | DTriple Data Data Data   deriving (Show)
data Error = TypeError String Data | MissingArg | ExtraArgs | PolyFunction deriving (Show)
                                
          
 -- can't do poly morphic functions atm          

 
class Arg a where 
    getArg :: Data -> Either Error a  
    wrap   :: a -> Data

instance Arg a where 
    getArg x = Left PolyFunction
    wrap a = DBool False --Temp
    
--Basic Literals    
instance Arg Int where
    getArg (DInt x) = Right x
    getArg       x  = Left $ TypeError "DInt" x
    wrap         x  = DInt x
instance Arg Bool where
    getArg (DBool x) = Right x
    getArg        x  = Left $ TypeError "DBool" x
    wrap          x  = DBool x
    
--Lists of literals
instance Arg [Int] where
    getArg (DListInt x) = Right x
    getArg           x  = Left $ TypeError "DListInt" x
    wrap             x  = DListInt x
instance Arg [Bool] where
    getArg (DListBool x) = Right x
    getArg           x   = Left $ TypeError "DListBool" x
    wrap             x   = DListBool x
    
-- 2 and 3 tuples
instance (Arg a, Arg b) => Arg (a, b) where
    getArg (DPair x y) = do
                      x' <- getArg x
                      y' <- getArg y
                      Right (x', y')
    getArg        x    = Left $ TypeError "DPair" x
    wrap        (x, y) = DPair (wrap x) (wrap y)
instance (Arg a, Arg b, Arg c) => Arg (a, b, c) where
    getArg (DTriple x y z) = do
                      x' <- getArg x
                      y' <- getArg y
                      z' <- getArg z
                      Right (x', y', z')
    getArg       x        = Left $ TypeError "DTriple" x
    wrap        (x, y, z) = DTriple (wrap x) (wrap y) (wrap z)


--Function class    
class Function a where
    exec :: a -> [Data] -> Either Error Data  
       
instance Arg a => Function a where
    exec x [] = Right (wrap x)
    exec _ _  = Left ExtraArgs     
   
--dodgy overlapping tag works for now look better answer
instance {-# OVERLAPPING #-} (Function f, Arg a) => Function (a -> f) where
    exec f (x:xs)  = do
                  x' <- getArg x
                  exec (f x') xs
    exec _ []       = Left $ MissingArg     

--TEST FUNCTIONS
    
logfoo :: Bool -> Bool -> Bool
logfoo x y = x

foo' :: Int -> Int -> Int 
foo' x y = x + y 

foo1 :: (Int, Int, Int) -> Bool -> Int
foo1 (x, y, z) p = if p then x * y * z else x + y + z

foo2 :: (Int, Int) -> Int
foo2 (x , y) = x + y

foo3 :: Int -> [Int]
foo3 x = [x, x, x]

foo4 :: Char -> Char 
foo4 x = 'C'
 
bar :: (Bool, Int) -> Bool -> Int
bar (x , y) p = if p then 2 * y else y

bar' :: Int -> (Int, Int)
bar' x = (x + 10, x * 10)

q :: Int -> a -> Int
q x y = x
    
instance Show Func where
    show s = "Function"
    