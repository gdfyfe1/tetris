import qualified Data.Sequence as Seq

data Type = TVar String
          | TLit TLit
    deriving (Show, Eq)

data TLit = TBool
          | TInt  
    deriving (Show, Eq)
     
data FunLine = FunLine (Seq.Seq Type , Type)
    deriving (Show, Eq)
    
class TypeUpdate a where
  update :: a -> Type -> String -> a
  
instance TypeUpdate Type where
  update  (TVar n) newT str  | n == str   = newT 
                            | otherwise  = (TVar n)
  update  t     _   _                    = t
  
instance TypeUpdate a => TypeUpdate (Seq.Seq a) where
  update seq newT str = case Seq.viewl seq of
                         Seq.EmptyL  -> Seq.empty
                         (x Seq.:< xs) -> update x newT str Seq.<| update xs newT str 

instance TypeUpdate FunLine where
  update (FunLine (t , out)) newType str  =  (FunLine((update t newType str ) , (update out newType str)))
  
updateHoleInt :: FunLine -> Int -> Type -> Maybe FunLine
updateHoleInt (FunLine (ts , out)) i newT = case  (ts `Seq.index` i) of
                                              t@(TVar n) -> case fun t of
                                                              Nothing -> Nothing 
                                                              Just f  -> Just (update f newT n)
                                              t          -> fun t
  where fun t = case apply newT t  of
                  Nothing -> Nothing
                  Just t' -> Just (FunLine((Seq.update i t' ts), out))

-- case ts `Seq.index` i of
--t@(TVar n)  -> Just (update (FunLine((Seq.update i (apply newT t) ts), out)) t n)
-- t           -> Just (       FunLine((Seq.update i (apply newT t) ts), out)) 

-- i >= 0 && i < length t = case apply newT

-- NewType -> HoleType  
apply :: Type -> Type -> Maybe Type
apply (TVar n)  (TVar m)              = Just (TVar n)
apply (TLit t)  (TVar n)              = Just (TLit t)
apply (TLit t1) (TLit t2) | t1 == t2  = Just (TLit t1)
                          | otherwise = Nothing
apply (TVar n)  (TLit t)              = Nothing

x :: FunLine
x = (FunLine( Seq.fromList [(TVar "a") , (TLit TBool) , (TVar "b"), (TVar "b")] , (TVar "b")))